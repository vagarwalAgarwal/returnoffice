import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {
  IPropertyPaneConfiguration,
  PropertyPaneTextField
} from '@microsoft/sp-property-pane';
import { BaseClientSideWebPart } from '@microsoft/sp-webpart-base';
import { IListService, ListService } from "../services/SPListServices";
import * as strings from 'ReturnOfficeWebPartStrings';
import ReturnOffice from './components/ReturnOffice';
import { IReturnOfficeProps } from './components/IReturnOfficeProps';
import { get, update } from '@microsoft/sp-lodash-subset';
import { PropertyFieldListPicker, PropertyFieldListPickerOrderBy } from '@pnp/spfx-property-controls/lib/PropertyFieldListPicker';
import { IPropertyFieldGroupOrPerson, PrincipalType, PropertyFieldPeoplePicker } from '@pnp/spfx-property-controls/lib/PropertyFieldPeoplePicker';

export interface IReturnOfficeWebPartProps {
  description: string;
  RequestList:string;
  ARGroupID:IPropertyFieldGroupOrPerson[];
  OfficeGroupID:IPropertyFieldGroupOrPerson[];
  Question1Status:string;
  Question2Status:string;
  Question3Status:string;
  Question4Status:string;
  Question5Status:string;
  Question6Status:string;
  Question7Status:string;
  pageTitle:string;
}

export default class ReturnOfficeWebPart extends BaseClientSideWebPart<IReturnOfficeWebPartProps> {
  private ListServices: IListService;
  protected onInit(): Promise<void> {
    this.ListServices = new ListService(this.context);
   
    return super.onInit();
  }
  public render(): void {
    const element: React.ReactElement<IReturnOfficeProps> = React.createElement(
      ReturnOffice,
      {
        description: this.properties.description,
        RequestList:this.properties.RequestList,
        context:this.context,
        listServices:this.ListServices,
        ARGroupID:this.properties.ARGroupID,
        OfficeGroupID:this.properties.OfficeGroupID,
        Question1Status:this.properties.Question1Status,
        Question2Status:this.properties.Question2Status,
        Question3Status:this.properties.Question3Status,
        Question4Status:this.properties.Question4Status,
        Question5Status:this.properties.Question5Status,
        Question6Status:this.properties.Question6Status,
        Question7Status:this.properties.Question7Status,
        pageTitle:this.properties.pageTitle
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }
  private onCustomPropertyPaneChange(propertyPath: string, newValue: any): void {
    // Log.verbose(this.logSource, "WebPart property '" + propertyPath + "' has changed, refreshing WebPart...", this.context.serviceScope);
    const oldValue = get(this.properties, propertyPath);
    // Stores the new value in web part properties
    update(this.properties, propertyPath, (): any => { return newValue; });
    this.onPropertyPaneFieldChanged(propertyPath, oldValue, newValue);
    this.render();
  }
  // protected get dataVersion(): Version {
  //   return Version.parse('1.0');
  // }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyPaneTextField('description', {
                  label: strings.DescriptionFieldLabel
                }),
                PropertyFieldListPicker('RequestList', {
                  label: "Select Required List",
                  selectedList: this.properties.RequestList,
                  includeHidden: false,
                  baseTemplate: 100,                  
                  disabled: false,
                  onPropertyChange: this.onCustomPropertyPaneChange.bind(this),
                  properties: this.properties,
                  context: this.context,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  key: 'listPickerRequestFieldId'
                }),
                PropertyFieldPeoplePicker('ARGroupID', {
                  label: "Return To Office Admin SharePointGroup",
                  initialData: this.properties.ARGroupID,
                  allowDuplicate: false,
                  principalType: [PrincipalType.SharePoint],
                  onPropertyChange: this.onCustomPropertyPaneChange.bind(this),
                  context: this.context,
                  properties: this.properties,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  multiSelect: false,
                  key: 'peoplePickerCPDGroupFieldId'
                }),
                PropertyFieldPeoplePicker('OfficeGroupID', {
                  label: "Office Assistant SharePointGroup",
                  initialData: this.properties.OfficeGroupID,
                  allowDuplicate: false,
                  principalType: [PrincipalType.SharePoint],
                  onPropertyChange: this.onCustomPropertyPaneChange.bind(this),
                  context: this.context,
                  properties: this.properties,
                  onGetErrorMessage: null,
                  deferredValidationTime: 0,
                  multiSelect: false,
                  key: 'peoplePickerCPDGroupFieldId'
                }),
                PropertyPaneTextField('Question1Status', {
                  label: "Question 1 Label"
                }),
                PropertyPaneTextField('Question2Status', {
                  label: "Question 2 Label"
                }),
                PropertyPaneTextField('Question3Status', {
                  label: "Question 3 Label"
                }),
                PropertyPaneTextField('Question4Status', {
                  label: "Question 4 Label"
                }),
                PropertyPaneTextField('Question5Status', {
                  label: "Question 5 Label"
                }),
                PropertyPaneTextField('Question6Status', {
                  label: "Question 6 Label"
                }),
                PropertyPaneTextField('Question7Status', {
                  label: "Question 7 Label"
                }),
                PropertyPaneTextField('pageTitle', {
                  label: "Page Title"
                }),
              ]
            }
          ]
        }
      ]
    };
  }
}
