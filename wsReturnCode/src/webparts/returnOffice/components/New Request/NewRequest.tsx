import * as React from 'react';
import styles from "./NewRequest.module.scss";
import { INewRequestProps, ICarrierOptions } from "./INewRequestProps";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Button, ButtonGroup, FormControlLabel, InputLabel, FormControl, OutlinedInput, InputAdornment } from "@material-ui/core";
import { CircularProgress, TextField, Checkbox } from '@material-ui/core';
import { TableContainer, Table, TableBody, TableCell, TableHead, TableRow, Paper, Tooltip, Typography, Backdrop, TableFooter } from '@material-ui/core';
import { escape } from '@microsoft/sp-lodash-subset';
import { default as pnp, ItemAddResult, Items } from "sp-pnp-js";
import { BillingCompany } from "../../../../Services/BillingCompany";
import { IReturnToOfficeCreateState, Ioption } from "../../../../Model/IReturnToOffice";
// import { Create, Delete, Info, Label, Warning } from '@material-ui/icons';
import { Create, Delete, Info, Warning } from '@material-ui/icons';
import { Label } from 'office-ui-fabric-react/lib/Label';
import  "../../../../CSS/CustomCss2.css"; 

// import Dropzone from "react-dropzone";
import '../../../../CSS/customCss.scss';
import { ChoiceGroup, IChoiceGroupOption } from 'office-ui-fabric-react/lib/ChoiceGroup';
// import { Label } from 'office-ui-fabric-react/lib/Label';
import { DatePicker, Icon, PrimaryButton } from 'office-ui-fabric-react';
import * as $ from 'jquery';
import { Dropdown, DropdownMenuItemType, IDropdownStyles, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
// import { TextField, ITextFieldStyles } from 'office-ui-fabric-react/lib/TextField';
import { SPComponentLoader } from '@microsoft/sp-loader';
import { WebPartContext } from "@microsoft/sp-webpart-base";
import { SPHttpClient, SPHttpClientResponse, ISPHttpClientOptions } from '@microsoft/sp-http';
import { HttpClient, HttpClientResponse } from '@microsoft/sp-http';
// import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import 'jquery-ui';
import { RichText } from "@pnp/spfx-controls-react/lib/RichText";
import { PeoplePicker, PrincipalType } from "@pnp/spfx-controls-react/lib/PeoplePicker";
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
// import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-step-progress-bar/styles.css';
import renderHTML from 'react-render-html';
import { ListItemPicker } from '@pnp/spfx-controls-react/lib/listItemPicker';
import Select from 'react-select';

var billcomp = new BillingCompany();
let currentUserEmail = "";
var NotesServiceTeamCommentsText, NotesFinanceTeamcommentsText: string;
//let IssueTypeOps: IDropdownOption[] = [];
let StatusOps: IDropdownOption[] = [];
let mainListProps = "";
var uploadedDocsList = [];
let AttachmentsHTML: any[] = [];
let userTitle = "";
let output, Configoutput: any;

const AssignedtoEpicops: IChoiceGroupOption[] = [
    { key: 'Yes', text: 'Yes' },
    { key: 'No', text: 'No' },
];
const ddlOptions: IDropdownOption[] = [
    { key: "Yes", text: "Yes"  },
    { key: "No", text: "No" },
  ];
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            "& .MuiTextField-root": {
                margin: theme.spacing(1),
                width: 200
            },

            button: {
                marginTop: theme.spacing(1),
                marginRight: theme.spacing(1)
            },
            actionsContainer: {
                marginBottom: theme.spacing(2)
            },
            resetContainer: {
                padding: theme.spacing(3)
            }
        }
    })
);
const backdropStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            xolor: '#fff'
        }
    }));

var uploadedDocsList = [];
export class NewRequest extends React.Component<INewRequestProps, IReturnToOfficeCreateState> {
    public issueTypeOps: IDropdownOption[] = [];
    public clientOptions = [];
    public dummyElementConfig = document.createElement("Div");

    constructor(props: INewRequestProps, state: IReturnToOfficeCreateState) { // added later: to initalize state
        super(props);
        // SPComponentLoader.loadScript('https://code.jquery.com/jquery-1.12.4.js');
        // SPComponentLoader.loadCss('https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
        // SPComponentLoader.loadScript('https://code.jquery.com/ui/1.12.1/jquery-ui.js');
        this.state = {
            items: [],
            isAlreadyOpted: false,
            firstName: "",
            Ques1: null,
            Ques2: "",
            Ques3: "",
            Ques4: false,
            Ques5: "",
            Ques6: "",
            Ques7: false,

            Ques1Status: true,
            Ques2Status: true,
            Ques3Status: true,
            Ques4Status: true,
            Ques5Status: true,
            Ques6Status: true,
            Ques7Status: true,

            ddlQues1Status: "",
            ddlQues2Status: "",
            ddlQues3Status: "",
            ddlQues4Status: "",
            ddlQues5Status: "",
            ddlQues6Status: "",
            ddlQues7Status: "",

            isHideQues1Status: false,
            isHideQues2Status: false,
            isHideQues3Status: false,
            isHideQues4Status: false,
            isHideQues5Status: false,
            isHideQues6Status: false,
            isHideQues7Status: false,

            AttestedQues1: false,
            AttestedQues2: false,
            AttestedQues3: false,
            AttestedQues4: false,
            AttestedQues5: false,
            AttestedQues6: false,
            AttestedQues7: false,
            AttestedQues8: false,
            AttestedQues9: false,
            additionalComments:"",
            iAgree: false,
            isLoadingImage: false,
            isRejected: false,
            isCollapse: false,
            isFinanceCollapse: false,
            selectedvalue: null,
            optionClient: [],
            isDataSelected: false,
            selectedReqDate: null,
            isARMember: false,
            isOfficeAdmin: false,
            Comments: "",
            isApproved: false,
            badgeID: "",
            isCompleted: false,
            isSubmitted: false,
            Signature:"",
            SubmitDate:null,
            previousDocument:[],
            isCreatedBy: false,
            IsEditEnabled:false,
            isdataLoaded:false,
            Office:""
            // IssueTypeOps:[],
        }
        //  this._getApproverPeoplePickerItems = this._getApproverPeoplePickerItems.bind(this);
        //this.handleChange = this.handleChange.bind(this);

    }
    public async componentDidMount() {
        pnp.setup({
            sp: {
                baseUrl: this.props.context.pageContext.web.absoluteUrl
            },
        });
        mainListProps = this.props.RequestListID;
        
        if (mainListProps == "" || mainListProps == undefined) {
            alert("Plese provide list name");
        }
        var ConfigData = "";// await sdk.getPackConfig("FloorPlan", currentUserEmail[0]["Email"]);
        var ConfigDataDetais = await billcomp.getConfigListData("ConfigList");
        if (ConfigDataDetais.length > 0) {
            this.dummyElementConfig.innerHTML = ConfigDataDetais[0].HeaderDetail
            Configoutput = this.dummyElementConfig.innerHTML;
        }
        currentUserEmail = await billcomp.getCurrentUserDetail();
        userTitle = currentUserEmail[0]["Title"];
        //below function is used to get Segment Master Record and bind in Drop Down
        //StatusOps = await billcomp.getStatusItems();
        //below function is used to get Issue Type Master Record and bind in Drop Down
        // IssueTypeOps=[];
        // this.issueTypeOps = await billcomp.getIssueTypeItems();
        this.setState({ firstName: userTitle });
        this.setState({ SubmitDate: new Date() })
    }

    private _onFormatDate = (date: Date): string => {
        // return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    }
    private handleChangeRetDate(e) {
        this.setState({ selectedReqDate: e });
    }
    private handleChangeQues1(e) {
        this.setState({ Ques1: e });
    }
    private handleChangeQues2(e) {
        this.setState({ Ques2: e.target.value });
    }
    private handleChangeQues3(e) {
        this.setState({ Ques3: e.target.value });
    }
    private handleChangeQues4(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ Ques4: true });
        }
        else {
            this.setState({ Ques4: false });
        }
    }
    private handleChangeQues5(e) {
        this.setState({ Ques5: e.target.value });
    }
    private handleChangeQues6(e) {
        this.setState({ Ques6: e.target.value });
    }  
    private handleChangeSignature(e) {
        this.setState({ Signature: e.target.value });
    } 
    private handleChangeOffice(e) {
        this.setState({ Office: e.target.value });
    } 
    private handleChangeSubmittedDate(e) {
        this.setState({ SubmitDate: e });
    }
    private handleChangeadditionalComments(e) {
        this.setState({ additionalComments: e.target.value });
    }
    private handleChangeQues7(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ Ques7: true });
        }
        else {
            this.setState({ Ques7: false });
        }
    }
    private fnQues1Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques1Status: true });
            this.setState({ isHideQues1Status: false });
        }
        else {
            this.setState({ Ques1Status: false });
            this.setState({ isHideQues1Status: true });
            this.setState({ Ques1: null});
        }
      }
      private fnQues2Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques2Status: true });
            this.setState({ isHideQues2Status: false });
        }
        else {
            this.setState({ Ques2Status: false });
            this.setState({ isHideQues2Status: true });
            this.setState({ Ques2: "" });
        }
      }
      private fnQues3Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques3Status: true });
            this.setState({ isHideQues3Status: false });
        }
        else {
            this.setState({ Ques3Status: false });
            this.setState({ isHideQues3Status: true });
            this.setState({ Ques3: "" });
        }
      }
      private fnQues4Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques4Status: true });
            this.setState({ isHideQues4Status: false });
        }
        else {
            this.setState({ Ques4Status: false });
            this.setState({ isHideQues4Status: true });
            //this.setState({ Ques4: "" });
        }
      }
      private fnQues5Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques5Status: true });
            this.setState({ isHideQues5Status: false });
        }
        else {
            this.setState({ Ques5Status: false });
            this.setState({ isHideQues5Status: true });
            this.setState({ Ques5: "" });
        }
      }
      private fnQues6Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques6Status: true });
            this.setState({ isHideQues6Status: false });
        }
        else {
            this.setState({ Ques6Status: false });
            this.setState({ isHideQues6Status: true });
            this.setState({ Ques6: "" });
        }
      }
      private fnQues7Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques7Status: true });
        }
        else {
            this.setState({ Ques7Status: false });
            this.setState({ isHideQues7Status: false });
            //this.setState({ Ques7: "" });
        }
      }
    private handleChangeAttestedQues1(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues1: true });
        }
        else {
            this.setState({ AttestedQues1: false });
        }
    }
    private handleChangeAttestedQues2(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues2: true });
        }
        else {
            this.setState({ AttestedQues2: false });
        }
    }
    private handleChangeAttestedQues3(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues3: true });
        }
        else {
            this.setState({ AttestedQues3: false });
        }
    }
    private handleChangeAttestedQues4(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues4: true });
        }
        else {
            this.setState({ AttestedQues4: false });
        }
    }
    private handleChangeAttestedQues5(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues5: true });
        }
        else {
            this.setState({ AttestedQues5: false });
        }
    }
    private handleChangeAttestedQues6(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues6: true });
        }
        else {
            this.setState({ AttestedQues6: false });
        }
    }
    private handleChangeAttestedQues7(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues7: true });
        }
        else {
            this.setState({ AttestedQues7: false });
        }
    }
    private handleChangeAttestedQues8(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues8: true });
        }
        else {
            this.setState({ AttestedQues8: false });
        }
    }
    private handleChangeAttestedQues9(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues9: true });
        }
        else {
            this.setState({ AttestedQues9: false });
        }
    }
    private handleChangeAgree(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ iAgree: true });
        }
        else {
            this.setState({ iAgree: false });
        }
    }
    public async SaveBankDetailsToMainList() {
        try {
            if (mainListProps == "" || mainListProps == undefined) {
                alert("Plese provide list name");
            }
           else if (this.state.Office == "" || this.state.selectedReqDate == null) {
                alert("Plese fill mandatory field");
            }
            else if (this.state.Ques1Status == true && this.state.Ques1 == null) {
             alert("Please enter Covid-19 date");
            }
            else if (this.state.Ques2Status == true && this.state.Ques2 == "") {
                alert("Please enter all mandatory field");
               }
               else if (this.state.Ques3Status == true && this.state.Ques3 == "") {
                alert("Please enter all mandatory field");
               }
               else if (this.state.Ques5Status == true && this.state.Ques5 == "") {
                alert("Please enter all mandatory field");
               }
               else if (this.state.Ques6Status == true && this.state.Ques6 == "") {
                alert("Please enter all mandatory field");
               }
            else if (this.state.AttestedQues1 == false || this.state.AttestedQues2 == false || this.state.AttestedQues3 == false || this.state.AttestedQues4 == false ||
                this.state.AttestedQues5 == false || this.state.AttestedQues6 == false || this.state.AttestedQues7 == false || this.state.AttestedQues8 == false ||
                this.state.AttestedQues9 == false) {
                alert("Please select all Attestation")
            }
            // if (this.state.firstName == "" || this.state.PaymentMethodCode == "" || this.state.nameOnAccount == "" || this.state.accountNumber == ""
            //   || this.state.countryOfOperation == ""
            //   || this.state.routingNumber == "" || this.state.serviceClassCode == ""
            // ) {
            //   alert("Please enter all mandatory field(s)")
            // }
            else if (this.state.iAgree == false) {
                alert("Please agree the above provide information")
            }
            else if (this.state.Signature == "") {
                alert("Please provide your signatue")
            }
            else if (this.state.SubmitDate == null) {
                alert("Please provide date")
            }
            // else if (this.state.instittionPhoneNumber.length < 10 && this.state.instittionPhoneNumber != "" && this.state.instittionPhoneNumber != null) {
            //   alert("Length of Institution Phone Number should be correct")
            // }
            else {
                let resultID =
                    await billcomp.saveDataToAddCompanyList(userTitle,this.state.additionalComments, this.state.Ques1, this.state.Ques2,
                        this.state.Ques3, this.state.Ques4Status, this.state.Ques5,this.state.Ques6, this.state.Ques7Status,
                        this.state.Ques1Status,this.state.Ques2Status,this.state.Ques3Status,this.state.Ques5Status,
                        this.state.Ques6Status,this.state.AttestedQues1,this.state.AttestedQues2, this.state.AttestedQues3, this.state.AttestedQues4,
                        this.state.AttestedQues5, this.state.AttestedQues6, this.state.AttestedQues7,
                        this.state.AttestedQues8, this.state.AttestedQues9, this.state.iAgree, this.state.selectedReqDate,this.state.Signature
                        ,this.state.SubmitDate,this.state.Office, mainListProps);
                if (resultID != "") {
                    try {
                        // let docListdata = await this.submitAttachments(resultID);
                        // console.log(docListdata);
                        //alert("Thank you for your response, your request has been sent for Approval");
                        window.location.hash = 'requests/all';
                        // location.href = context.pageContext.web.absoluteUrl;
                    }
                    catch (error) {
                        alert("error while uploading the document")
                    }
                    //  await addInsurance.getEmployeeList(currentUserEmail[0]["Email"]);

                }
            }
        }
        catch (error) {
            alert("error while inserting the item")
        }

    }
    public _onCancel() {
        window.location.hash = 'requests/all';
    }


    // adding documents
    // private submitAttachments = async (listItemID) => {
    //     try {
    //         let item = await pnp.sp.web.lists.getById(mainListProps).items.getById(listItemID);
    //         for (let i = 0; this.state.uploadDocInfo.length; i++) {
    //             let add = await item.attachmentFiles.add(this.state.uploadDocInfo[i]["name"], this.state.uploadDocInfo[i]["content"]);
    //         }
    //     }
    //     catch (error) {
    //         console.log(error);
    //     }
    // }
    // public render(): React.ReactElement<INewRequestProps> 
    public render(): React.ReactElement<INewRequestProps> {
        const options = {
            noDataText: 'No Data Found'
        };
        let classes: any = useStyles;
        let backDropClasses: any = backdropStyles;
        const rowStyle = (row, rowIndex) => {
            const style = {};
            if (rowIndex % 2 === 0) {
                return { backgroundColor: '#c8e6c9' }
            }
            else {
                style: "green";
            }
            return style;// rowIndex % 2 === 0 ? style: "red" : style "green";
        };
        const value = this.state.selectedvalue;
        const ListColumn = [
            {
                dataField: 'Department', text: 'Department'
            },
            {
                dataField: 'AHName', text: 'Account Handler'
            },
        ]
        return (
            <div className={styles.wsclientrefund}>
                {/* <div className='divHeader'>{this.props.context}</div> */}
                <br />
                <div className="">
                    <div className={classes.root}>
                        <div className="divclear">
                            <Backdrop className={backDropClasses.backdrop} style={{ zIndex: 9 }} open={this.state.isLoadingImage}>
                                <CircularProgress color="inherit" />
                            </Backdrop>
                        </div>

                        <div className={styles.boxRow}>
                            <div className="dvConfigData">
                                <div dangerouslySetInnerHTML={{ __html: Configoutput }}></div>
                            </div>
                            <br />
                            <div className="dvFirstHeader" style={{float:"left",width:"98%"}}>
                                <div className={styles.columnrow} style={{ paddingTop: "10px" }}>
                                    <Label className="divMainHeader" >Return to Office Questionnaire</Label>
                                </div>
                                <div className="divclear">
                                    <div className={styles.columnrow}>
                                        <Label style={{ float: "left", paddingRight: "10px",width:"135px" }} className="divCommon">Name:</Label>
                                        <div style={{ paddingTop: "7px" }} className="">{this.state.firstName}</div>
                                    </div>
                                    
                                    <div className={styles.columnrow}>
                                        <Label style={{ float: "left", paddingRight: "10px", paddingTop: "8px",width:"135px" }} className="divCommon">Requested Date:<span className="spnRequired">*</span> </Label>
                                        <div style={{ paddingTop: "7px", width: "28%" }} >
                                            <DatePicker placeholder="Select a date..."
                                                value={this.state.selectedReqDate}
                                                formatDate={this._onFormatDate}
                                                onSelectDate={(e) => this.handleChangeRetDate(e)}
                                                disabled={this.state.isDataSelected}
                                            /></div>
                                    </div>
                                    <div className={styles.columnrow}>
                                    <Label style={{ float: "left", paddingRight: "10px", paddingTop: "8px",width:"135px" }} className="divCommon">Office:<span className="spnRequired">*</span> </Label>
                                        <div style={{ paddingTop: "7px", float: "left",width: "40%" }} >
                                            <TextField
                                                id="office"
                                                name="Office"
                                                label="Office"
                                                variant="outlined"
                                                size="small"
                                                required
                                                fullWidth
                                                onChange={(e) => this.handleChangeOffice(e)}
                                                value={this.state.Office}
                                            /></div>
                                    </div>
                                </div>
                                <div className={styles.columnrow}> <TextField
                                    id="addComments"
                                    name="addComments"
                                    label="Additional Comments"
                                    variant="outlined"
                                    size="small"
                                    rows={5}
                                    multiline
                                    fullWidth
                                    onChange={(e) => this.handleChangeadditionalComments(e)}
                                    value={this.state.additionalComments}
                                /></div>
                                <div style={{paddingTop: "5px", clear: "both"}} >
                                    <hr className="divHr"/>
                                    </div>
                                <div className={styles.columnrow} >
                                    <div style={{ width: "85%", float: "left" }}><Label className="divCommon">{this.props.Question1Status}</Label></div>
                                    <div style={{ width: "15%", float: "left", paddingBottom:"10px" }}>
                                        <Dropdown
                                            placeholder="Select an option"
                                            options={ddlOptions}
                                            defaultSelectedKey="Yes"
                                            onChange={this.fnQues1Status.bind(this)}
                                        />
                                    </div>
                                  {!this.state.isHideQues1Status &&  <div style={{ width: "20%" }}>
                                        <DatePicker  placeholder="Select a date..."
                                            onSelectDate={(e) => this.handleChangeQues1(e)}
                                            value={this.state.Ques1}
                                            formatDate={this._onFormatDate}
                                        />
                                    </div>}
                                </div>
                                
                                
                                
                                <div className={styles.columnrow}>
                                    <div style={{ width: "85%", float: "left" }}><Label className="divCommon">{this.props.Question2Status} <span className="spnRequired">*</span></Label></div>
                                    <div style={{ width: "15%", float: "left", paddingBottom:"10px" }}>
                                        <Dropdown
                                            placeholder="Select an option"
                                            options={ddlOptions}
                                            defaultSelectedKey="Yes"
                                            onChange={this.fnQues2Status.bind(this)}
                                        />
                                    </div>
                                    {!this.state.isHideQues2Status &&   <div > <TextField
                                        id="Ques2"
                                        name="Ques2"
                                        label=""
                                        variant="outlined"
                                        size="small"
                                        rows={5}
                                        multiline
                                        fullWidth
                                        onChange={(e) => this.handleChangeQues2(e)}
                                        value={this.state.Ques2}
                                    /></div>}

                                </div>
                                <div className={styles.columnrow}>
                                    <div style={{ width: "85%", float: "left" }}> <Label className="divCommon">{this.props.Question3Status}<span className="spnRequired">*</span></Label></div>
                                    <div style={{ width: "15%", float: "left", paddingBottom:"10px" }}>
                                        <Dropdown
                                            placeholder="Select an option"
                                            options={ddlOptions}
                                            defaultSelectedKey="Yes"
                                            onChange={this.fnQues3Status.bind(this)}
                                        />
                                    </div>
                                    {!this.state.isHideQues3Status &&  <div >    <TextField
                                        id="Ques3"
                                        name="Ques3"
                                        label=""
                                        variant="outlined"
                                        size="small"
                                        rows={5}
                                        multiline
                                        fullWidth
                                        onChange={(e) => this.handleChangeQues3(e)}
                                        value={this.state.Ques3}
                                    />
                                    </div>}
                                </div>
                                <div className={styles.columnrow}>
                                    <div style={{ width: "85%", float: "left" }}> <Label className="divCommon">{this.props.Question4Status}</Label></div>
                                    <div style={{ width: "15%", float: "left" }}>
                                        <Dropdown
                                            placeholder="Select an option"
                                            options={ddlOptions}
                                            defaultSelectedKey="Yes"
                                            onChange={this.fnQues4Status.bind(this)}
                                        />
                                    </div>
                                    {/* <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.Ques4} onChange={this.handleChangeQues4.bind(this)} name="chkStatus" />}
                                        label=""
                                    /> */}
                                </div>
                                <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}>  <Label className="divCommon">{this.props.Question5Status}<span className="spnRequired">*</span></Label></div>
                                <div style={{ width: "15%", float: "left", paddingBottom:"10px" }}>
                                        <Dropdown
                                            placeholder="Select an option"
                                            options={ddlOptions}
                                            defaultSelectedKey="Yes"
                                            onChange={this.fnQues5Status.bind(this)}
                                        />
                                    </div>
                                    {!this.state.isHideQues5Status &&  <div>
                                        <TextField
                                            id="Ques5"
                                            name="Ques5"
                                            label=""
                                            variant="outlined"
                                            size="small"
                                            fullWidth
                                            rows={5}
                                            multiline
                                            onChange={(e) => this.handleChangeQues5(e)}
                                            value={this.state.Ques5}
                                        />
                                    </div>}
                                </div>
                                <div className={styles.columnrow}>
                                    <div style={{ width: "85%", float: "left" }}>   <Label className="divCommon">{this.props.Question6Status}<span className="spnRequired">*</span></Label></div>
                                    <div style={{ width: "15%", float: "left", paddingBottom:"10px" }}>
                                        <Dropdown
                                            placeholder="Select an option"
                                            options={ddlOptions}
                                            defaultSelectedKey="Yes"
                                            onChange={this.fnQues6Status.bind(this)}
                                        />
                                    </div>
                                    {!this.state.isHideQues6Status && <div>
                                        <TextField
                                            id="Ques6"
                                            name="Ques6"
                                            label=""
                                            variant="outlined"
                                            size="small"
                                            fullWidth
                                            rows={5}
                                            multiline
                                            onChange={(e) => this.handleChangeQues6(e)}
                                            value={this.state.Ques6}
                                        />
                                    </div>}
                                </div>
                                <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}><Label className="divCommon">{this.props.Question7Status}</Label></div>
                                <div style={{ width: "15%", float: "left" }}>
                                        <Dropdown
                                            placeholder="Select an option"
                                            options={ddlOptions}
                                            defaultSelectedKey="Yes"
                                            onChange={this.fnQues7Status.bind(this)}
                                        />
                                    </div>
                                    {/* <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.Ques7} onChange={this.handleChangeQues7.bind(this)} name="chkStatus" />}
                                        label="Would you be taking public transportation to the office?"
                                    /> */}
                                </div>
                            </div>
                            <div className="dvFirstHeader" style={{float:"left",width:"98%"}}>
                                <div className={styles.columnrow} style={{ paddingTop: "10px" }}>
                                    <Label className="divMainHeader">Attestation</Label>
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues1} onChange={this.handleChangeAttestedQues1.bind(this)} name="chkStatus" />}
                                        label="I will take my temperature daily before going into the office and will not go to the office if I have a temperature of 100.4 or over"
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues2} onChange={this.handleChangeAttestedQues2.bind(this)} name="chkStatus" />}
                                        label="I will not go into the office on any day I have symptoms of COVID-19, regardless of my temperature, or if I know or suspect I may have COVID-19"
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues3} onChange={this.handleChangeAttestedQues3.bind(this)} name="chkStatus" />}
                                        label="I will not go into the office on any day a member of my household is sick or has symptoms of COVID-19"
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues4} onChange={this.handleChangeAttestedQues4.bind(this)} name="chkStatus" />}
                                        label="I will not go into the office if I am awaiting COVID-19 test results, or if I have been advised by a health care provider or local public health department to self-quarantine "
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues5} onChange={this.handleChangeAttestedQues5.bind(this)} name="chkStatus" />}
                                        label="I will practice social distancing and maintain at least six feet of distance from others while in the workplace whenever possible"
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues6} onChange={this.handleChangeAttestedQues6.bind(this)} name="chkStatus" />}
                                        label="I will wear a mask in the office at all times, except when seated at my desk"
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues7} onChange={this.handleChangeAttestedQues7.bind(this)} name="chkStatus" />}
                                        label="I will wear a face covering as required by law, including in any public areas or when social distancing (maintaining at least 6 feet of distance) is not practical or possible"
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues8} onChange={this.handleChangeAttestedQues8.bind(this)} name="chkStatus" />}
                                        label="I agree to notify Elyse Douglas or Zac Overbay if I develop symptoms or believe I may have been exposed to COVID at any time after completing this questionnaire"
                                    />
                                </div>
                                <div className={styles.columnrow}>
                                    <FormControlLabel
                                        control={
                                            <Checkbox id="chkApprove" required checked={this.state.AttestedQues9} onChange={this.handleChangeAttestedQues9.bind(this)} name="chkStatus" />}
                                        label="I agree to notify Elyse Douglas or Zac Overbay if the answer to any of the questions above change at any time after completing this questionnaire"
                                    />
                                </div>

                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox id="chkApprove" required checked={this.state.iAgree} onChange={this.handleChangeAgree.bind(this)} name="chkStatus" />}
                                    label="I attest that all the information I have provided above is true and accurate.  Should any of this information change I recognize I am obligated to submit a new questionnaire."
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <div style={{ width: "40%", float: "left" }}>
                                    <TextField
                                    id="signa"
                                    name="sign"
                                    label="Signature"
                                    variant="outlined"
                                    size="small"
                                    fullWidth
                                    onChange={(e) => this.handleChangeSignature(e)}
                                    value={this.state.Signature}
                                /></div>
                                <div style={{ width: "20%", float: "right" }}> <DatePicker placeholder="Select a date..."
                                    onSelectDate={(e) => this.handleChangeSubmittedDate(e)}
                                    value={this.state.SubmitDate}
                                    formatDate={this._onFormatDate}
                                /></div>
                            </div>
                        </div>
                        <br />
                    </div>
                    <div className="divclear" style={{ minHeight: "20px" }}>
                        <div className="" style={{ padding: "6px", paddingLeft: "25px" }}>
                            <Button size="medium" color="default" variant="contained" onClick={(e) => { this._onCancel() }} className={classes.button} style={{ margin: '20px', maxWidth: '180px', maxHeight: '40px', minWidth: '180px', minHeight: '40px' }}>
                                <div style={{ marginLeft: '5px' }}>Cancel</div>
                            </Button>
                                &nbsp;&nbsp;&nbsp;  <Button size="medium" color="primary" variant="contained" onClick={(e) => { this.SaveBankDetailsToMainList() }} className={classes.button} style={{ margin: '20px', maxWidth: '180px', maxHeight: '40px', minWidth: '180px', minHeight: '40px' }}>
                                <div style={{ marginLeft: '5px' }}>Submit</div>
                            </Button>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}