import { WebPartContext } from "@microsoft/sp-webpart-base";
import { HttpClient } from "@microsoft/sp-http";  

export interface INewRequestProps {
    context?: WebPartContext;
    RequestListID?:string;
    Question1Status:string;
    Question2Status:string;
    Question3Status:string;
    Question4Status:string;
    Question5Status:string;
    Question6Status:string;
    Question7Status:string;
    //myhttpclient:HttpClient; 
}

export interface ICarrierOptions{
    title:string
}
