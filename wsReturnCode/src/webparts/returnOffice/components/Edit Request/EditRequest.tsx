
import * as React from 'react';
import { IEditRequestProps } from "./IEditRequestProps";
import styles from "./EditRequest.module.scss";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import { Button, ButtonGroup, FormControlLabel, InputLabel, FormControl, OutlinedInput, InputAdornment } from "@material-ui/core";
// import { NotificationContainer } from 'react-notifications';
//import 'react-notifications/lib/notifications.css';
// import { NotificationManager } from 'react-notifications';
import { CircularProgress, TextField, Checkbox } from '@material-ui/core';
import { TableContainer, Table, TableBody, TableCell, TableHead, TableRow, Paper, Tooltip, Typography, Backdrop, TableFooter } from '@material-ui/core';
import { escape } from '@microsoft/sp-lodash-subset';
import { default as pnp, ItemAddResult, Items } from "sp-pnp-js";
import { BillingCompany } from "../../../../Services/BillingCompany";
import { IReturnToOfficeCreateState, Ioption } from "../../../../Model/IReturnToOffice";
// import { Create, Delete, Info, Label, Warning } from '@material-ui/icons';
import { Create, Delete, Info, Warning } from '@material-ui/icons';
import { Label } from 'office-ui-fabric-react/lib/Label';
// import Dropzone from "react-dropzone";
import '../../../../CSS/customCss.scss';
import { ChoiceGroup, IChoiceGroupOption } from 'office-ui-fabric-react/lib/ChoiceGroup';
// import { Label } from 'office-ui-fabric-react/lib/Label';
import { DatePicker, Icon, PrimaryButton } from 'office-ui-fabric-react';
import * as $ from 'jquery';
import { Dropdown, DropdownMenuItemType, IDropdownStyles, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
// import { TextField, ITextFieldStyles } from 'office-ui-fabric-react/lib/TextField';
import { SPComponentLoader } from '@microsoft/sp-loader'
import { SPHttpClient, SPHttpClientResponse, ISPHttpClientOptions } from '@microsoft/sp-http';
import { HttpClient, HttpClientResponse } from '@microsoft/sp-http';
// import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import 'jquery-ui';
import Chip from '@material-ui/core/Chip';
import { RichText } from "@pnp/spfx-controls-react/lib/RichText";
import { PeoplePicker, PrincipalType } from "@pnp/spfx-controls-react/lib/PeoplePicker";
import BootstrapTable from 'react-bootstrap-table-next';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
// import paginationFactory from 'react-bootstrap-table2-paginator';
import 'react-step-progress-bar/styles.css';
import renderHTML from 'react-render-html';
import { ListItemPicker } from '@pnp/spfx-controls-react/lib/listItemPicker';
import Select from 'react-select';
import { Placeholder } from "@pnp/spfx-controls-react/lib/Placeholder";
import { UrlQueryParameterCollection } from '@microsoft/sp-core-library';

var billcomp = new BillingCompany();
let currentUserEmail = "";
var NotesServiceTeamCommentsText, NotesFinanceTeamcommentsText: string;
let IssueTypeOps: IDropdownOption[] = [];
let StatusOps: IDropdownOption[] = [];
let mainListProps = "";
var uploadedDocsList = [];
var previousDoclist = [];
let AttachmentsHTML: any[] = [];
var deleteAttachment = [];
var folderUrl = [];
let userTitle = "";
let outputSales, Configoutput, outputFinance, vrItemHistory: any;

const AssignedtoEpicops: IChoiceGroupOption[] = [
    { key: 'Yes', text: 'Yes' },
    { key: 'No', text: 'No' },
];
const ddlOptions: IDropdownOption[] = [
    { key: "Yes", text: "Yes" },
    { key: "No", text: "No" },
];
let getResults: any;
var itemid = null;

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            "& .MuiTextField-root": {
                margin: theme.spacing(1),
                width: 200
            },

            button: {
                marginTop: theme.spacing(1),
                marginRight: theme.spacing(1)
            },
            actionsContainer: {
                marginBottom: theme.spacing(2)
            },
            resetContainer: {
                padding: theme.spacing(3)
            }
        }
    })
);
const backdropStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            xolor: '#fff'
        }
    }));

export class EditRequest extends React.Component<IEditRequestProps, IReturnToOfficeCreateState> {
    public dummySalesElement = document.createElement("div");
    public dummyFinanceElement = document.createElement("div");
    public dummyItemHistoryElement = document.createElement("div");
    public dummyElementConfig = document.createElement("Div");

    // private ListServices: IListService;
    constructor(props: IEditRequestProps, state: IReturnToOfficeCreateState) { // added later: to initalize state
        super(props);
        this.state = {
            items: [],
            isAlreadyOpted: false,
            firstName: "",
            Ques1: null,
            Ques2: "",
            Ques3: "",
            Ques4: false,
            Ques5: "",
            Ques6: "",
            Ques7: false,

            Ques1Status: true,
            Ques2Status: true,
            Ques3Status: true,
            Ques4Status: true,
            Ques5Status: true,
            Ques6Status: true,
            Ques7Status: true,

            ddlQues1Status: "",
            ddlQues2Status: "",
            ddlQues3Status: "",
            ddlQues4Status: "",
            ddlQues5Status: "",
            ddlQues6Status: "",
            ddlQues7Status: "",
            additionalComments: "",
            isHideQues1Status: false,
            isHideQues2Status: false,
            isHideQues3Status: false,
            isHideQues4Status: false,
            isHideQues5Status: false,
            isHideQues6Status: false,
            isHideQues7Status: false,

            AttestedQues1: false,
            AttestedQues2: false,
            AttestedQues3: false,
            AttestedQues4: false,
            AttestedQues5: false,
            AttestedQues6: false,
            AttestedQues7: false,
            AttestedQues8: false,
            AttestedQues9: false,

            iAgree: false,
            isLoadingImage: false,
            isRejected: false,
            isCollapse: false,
            isFinanceCollapse: false,
            selectedvalue: null,
            optionClient: [],
            isDataSelected: true,
            selectedReqDate: null,
            isARMember: false,
            isOfficeAdmin: false,
            Comments: "",
            isApproved: false,
            badgeID: "",
            isCompleted: false,
            isSubmitted: false,
            Signature: "",
            SubmitDate: null,
            previousDocument: [],
            isCreatedBy: false,
            IsEditEnabled: false,
            isdataLoaded: false,
            Office: ""
        }
        // this._getApproverPeoplePickerItems = this._getApproverPeoplePickerItems.bind(this);
        // this.handleChange = this.handleChange.bind(this);
    }
    public async componentDidMount() {
        pnp.setup({
            sp: {
                baseUrl: this.props.context.pageContext.web.absoluteUrl
            },
        });
        // window.location.hash =
        //var queryParameters = new UrlQueryParameterCollection(window.location.href);
        itemid = this.props.ID;
        mainListProps = this.props.RequestListID;
        // if (queryParameters.getValue("cID")) {
        if (mainListProps == "" || mainListProps == undefined) {
            alert("Plese provide list name");
            //this.setState({ isDataReceived: false });
        }
        else {
            //  itemid = parseInt(queryParameters.getValue("cID"));
            getResults = await billcomp.getCompanyDetails(itemid, mainListProps);
            currentUserEmail = await billcomp.getCurrentUserDetail();
            userTitle = currentUserEmail[0]["Title"];
            //below function is used to get Status Master Record and bind in Drop Down
            //StatusOps = await billcomp.getStatusItems();
            //below function is used to get Issue Type Build Master Record and bind in Drop Down
            //  itemid = parseInt(queryParameters.getValue("cID"));

            var ConfigData = "";// await sdk.getPackConfig("FloorPlan", currentUserEmail[0]["Email"]);
            var ConfigDataDetais = await billcomp.getConfigListData("ConfigList");

            if (ConfigDataDetais.length > 0) {
                this.dummyElementConfig.innerHTML = ConfigDataDetais[0].HeaderDetail
                Configoutput = this.dummyElementConfig.innerHTML;
            }
            this.setState({ firstName: userTitle });
            //  If user is Created By        
            if (getResults[0].Author.Title == userTitle) {
                this.setState({ isCreatedBy: true, isDataSelected: false });
                // alert("createdby")
            }
            this.setState({
                firstName: getResults[0].Title,
                Ques1: getResults[0].householdpositiveCOVID19 != null ? new Date(this.getCurrentDateItem(getResults[0].householdpositiveCOVID19)) : null,
                Ques2: getResults[0].householdpositiveexhibitsymptom,
                Ques3: getResults[0].visitedCDCidentifiedriskfacility,
                Office: getResults[0].Office == null ? "" : getResults[0].Office,

                ddlQues1Status: getResults[0].Question1Status == true ? "Yes" : "No",
                isHideQues1Status: getResults[0].Question1Status,
                Ques1Status: getResults[0].Question1Status,
                ddlQues2Status: getResults[0].Question2Status == true ? "Yes" : "No",
                isHideQues2Status: getResults[0].Question2Status,
                Ques2Status: getResults[0].Question2Status,

                ddlQues3Status: getResults[0].Question3Status == true ? "Yes" : "No",
                isHideQues3Status: getResults[0].Question3Status,
                Ques3Status: getResults[0].Question3Status,

                Ques4: getResults[0].householdtraveledbyplanetrain,
                ddlQues4Status: getResults[0].householdtraveledbyplanetrain == true ? "Yes" : "No",
                Ques4Status: getResults[0].householdtraveledbyplanetrain,
                Ques5: getResults[0].householdtripoutsidehome,
                Ques6: getResults[0].householdattendlarger15people,

                ddlQues5Status: getResults[0].Question5Status == true ? "Yes" : "No",
                isHideQues5Status: getResults[0].Question5Status,
                Ques5Status: getResults[0].Question5Status,

                ddlQues6Status: getResults[0].Question6Status == true ? "Yes" : "No",
                isHideQues6Status: getResults[0].Question6Status,
                Ques6Status: getResults[0].Question6Status,

                additionalComments: getResults[0].AdditionalComments,
                Ques7: getResults[0].publictransportationoffice,
                ddlQues7Status: getResults[0].publictransportationoffice == true ? "Yes" : "No",
                AttestedQues1: getResults[0].taketemparaturedaily,
                AttestedQues2: getResults[0].notgoofficehavesymptomsCOVID19,
                AttestedQues3: getResults[0].notgoofficehouseholdsick,
                // CurrentStatus: getResults[0].CurrentStatus,
                AttestedQues4: getResults[0].notgoofficeCOVID19testresults,
                AttestedQues5: getResults[0].practicesocialdistance6feetother,
                AttestedQues6: getResults[0].wearmaskofficeseateddesk,
                AttestedQues7: getResults[0].facecoveringsocialdistancing,
                AttestedQues8: getResults[0].notifyElyseZacsymptomcovid,
                AttestedQues9: getResults[0].notifyElyseZacquestionchangetime,
                iAgree: getResults[0].iAgree,
                Comments: getResults[0].Comments == null ? "" : getResults[0].Comments,
                badgeID: getResults[0].BadgeID,
                Signature: getResults[0].Signature,
                SubmitDate: getResults[0].date != null ? new Date(this.getCurrentDateItem(getResults[0].date)) : null,
                selectedReqDate: getResults[0].ReturnDate != null ? new Date(this.getCurrentDateItem(getResults[0].ReturnDate)) : null,
            });

            if (getResults[0].Status == "Submitted" || getResults[0].Status == "Completed" || getResults[0].Status == "Approved") {
                this.setState({ isCompleted: true, isDataSelected: true })
            }


            let CCUserIdsColl = [];
            let CCEmailIdsColl = [];
            if (getResults[0].AccountHandler) {
                for (let i = 0; getResults[0].AccountHandler && i < getResults[0].AccountHandler.length; i++) {
                    CCEmailIdsColl.push(getResults[0].AccountHandler[i].EMail);
                    CCUserIdsColl.push(getResults[0].AccountHandler[i].Id);
                }
            }

            // if (getResults[0].AttachmentFiles.length > 0) {
            //     let attachInfo: any[] = [];
            //     getResults[0].AttachmentFiles.forEach(element => {
            //         let obj = {};
            //         var _ServerRelativeUrl = element.ServerRelativeUrl;
            //         let url: string = this.props.context.pageContext.web.absoluteUrl + "/_layouts/download.aspx?SourceUrl=" +
            //             encodeURIComponent(_ServerRelativeUrl);
            //         obj["Url"] = url;
            //         obj["fileName"] = element.FileName;
            //         attachInfo.push(obj);
            //     });
            //     this.setState({ previousDocument: attachInfo })
            //     //let fileInfo = await sdk.getAddCosmpanyFile(itemid, context.pageContext.web.absoluteUrl);

            // }
        }
        // this.dummySalesElement.innerHTML = this.state.SalesCommentsSummary;
        // outputSales = this.dummySalesElement.innerHTML;
        this.dummyFinanceElement.innerHTML = this.state.Comments;
        outputFinance = this.dummyFinanceElement.innerHTML;
        // this.dummyItemHistoryElement.innerHTML = this.state.itemHistorySummary;
        // vrItemHistory = this.dummyItemHistoryElement.innerHTML;

        this.setState({ isdataLoaded: true });
    }
    private getCurrentDateItem(currDate) {
        var date = new Date(currDate).getDate();
        var month = new Date(currDate).getMonth() + 1;
        var year = new Date(currDate).getFullYear();
        return month + '-' + date + '-' + year;
    }

    private _onFormatDate = (date: Date): string => {
        return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
    }
    private handleChangefirstName(e) {
        this.setState({ firstName: e.target.value });
    }
    private handleChangeRetDate(e) {
        this.setState({ selectedReqDate: e });
    }
    private handleChangeQues1(e) {
        this.setState({ Ques1: e });
    }
    private handleChangeQues2(e) {
        this.setState({ Ques2: e.target.value });
    }
    private handleChangeQues3(e) {
        this.setState({ Ques3: e.target.value });
    }
    private handleChangeQues4(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ Ques4: true });
        }
        else {
            this.setState({ Ques4: false });
        }
    }
    private handleChangeQues5(e) {
        this.setState({ Ques5: e.target.value });
    }
    private handleChangeQues6(e) {
        this.setState({ Ques6: e.target.value });
    }
    private handleChangeSignature(e) {
        this.setState({ Signature: e.target.value });
    }
    private handleChangeOffice(e) {
        this.setState({ Office: e.target.value });
    }
    private handleChangeSubmittedDate(e) {
        this.setState({ SubmitDate: e });
    }
    private handleChangeadditionalComments(e) {
        this.setState({ additionalComments: e.target.value });
    }
    private handleChangeQues7(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ Ques7: true });
        }
        else {
            this.setState({ Ques7: false });
        }
    }
    private fnQues1Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques1Status: true });
            this.setState({ isHideQues1Status: true, ddlQues1Status: "Yes" });

        }
        else {
            this.setState({ Ques1Status: false });
            this.setState({ isHideQues1Status: false });
            this.setState({ Ques1: null, ddlQues1Status: "No" });
        }
    }
    private fnQues2Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques2Status: true, ddlQues2Status: "Yes" });
            this.setState({ isHideQues2Status: true });
        }
        else {
            this.setState({ Ques2Status: false });
            this.setState({ isHideQues2Status: false });
            this.setState({ Ques2: "", ddlQues2Status: "No" });
        }
    }
    private fnQues3Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques3Status: true, ddlQues3Status: "Yes" });
            this.setState({ isHideQues3Status: true });
        }
        else {
            this.setState({ Ques3Status: false });
            this.setState({ isHideQues3Status: false });
            this.setState({ Ques3: "", ddlQues3Status: "No" });
        }
    }
    private fnQues4Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques4Status: true, ddlQues4Status: "Yes" });
            this.setState({ isHideQues4Status: false });
        }
        else {
            this.setState({ Ques4Status: false });
            this.setState({ isHideQues4Status: true, ddlQues4Status: "No" });
            //this.setState({ Ques4: "" });
        }
    }
    private fnQues5Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques5Status: true, ddlQues5Status: "Yes" });
            this.setState({ isHideQues5Status: true });
        }
        else {
            this.setState({ Ques5Status: false });
            this.setState({ isHideQues5Status: false });
            this.setState({ Ques5: "", ddlQues5Status: "No" });
        }
    }
    private fnQues6Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques6Status: true, ddlQues6Status: "Yes" });
            this.setState({ isHideQues6Status: true });
        }
        else {
            this.setState({ Ques6Status: false });
            this.setState({ isHideQues6Status: false });
            this.setState({ Ques6: "", ddlQues6Status: "No" });
        }
    }
    private fnQues7Status(event: React.FormEvent<HTMLDivElement>, option: IDropdownOption): void {
        if (option.key.toString() == "Yes") {
            this.setState({ Ques7Status: true, ddlQues7Status: "Yes" });
        }
        else {
            this.setState({ Ques7Status: false });
            this.setState({ isHideQues7Status: false, ddlQues7Status: "No" });
            //this.setState({ Ques7: "" });
        }
    }
    private handleChangeAttestedQues1(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues1: true });
        }
        else {
            this.setState({ AttestedQues1: false });
        }
    }
    private handleChangeAttestedQues2(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues2: true });
        }
        else {
            this.setState({ AttestedQues2: false });
        }
    }
    private handleChangeAttestedQues3(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues3: true });
        }
        else {
            this.setState({ AttestedQues3: false });
        }
    }
    private handleChangeAttestedQues4(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues4: true });
        }
        else {
            this.setState({ AttestedQues4: false });
        }
    }
    private handleChangeAttestedQues5(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues5: true });
        }
        else {
            this.setState({ AttestedQues5: false });
        }
    }
    private handleChangeAttestedQues6(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues6: true });
        }
        else {
            this.setState({ AttestedQues6: false });
        }
    }
    private handleChangeAttestedQues7(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues7: true });
        }
        else {
            this.setState({ AttestedQues7: false });
        }
    }
    private handleChangeAttestedQues8(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues8: true });
        }
        else {
            this.setState({ AttestedQues8: false });
        }
    }
    private handleChangeAttestedQues9(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ AttestedQues9: true });
        }
        else {
            this.setState({ AttestedQues9: false });
        }
    }
    private handleChangeAgree(ev: React.FormEvent<HTMLElement>, isChecked: boolean) {
        if (isChecked) {
            this.setState({ iAgree: true });
        }
        else {
            this.setState({ iAgree: false });
        }
    }

    //If request is rejected, then user will submit again
    public async UpdateBillDifferToMainList(vrStatus) {
        //this.setState({ isLoadingImage: true });
        try {

            if (mainListProps == "" || mainListProps == undefined) {
                alert("Plese provide list name");
            }
            else if (this.state.Office == "" || this.state.selectedReqDate == null) {
                alert("Please enter mandatory fields");
            }
            else if (this.state.Ques1Status == true && this.state.Ques1 == null) {
                alert("Please enter Covid-19 date");
            }
            else if (this.state.Ques2Status == true && this.state.Ques2 == "") {
                alert("Please enter all mandatory field");
            }
            else if (this.state.Ques3Status == true && this.state.Ques3 == "") {
                alert("Please enter all mandatory field");
            }
            else if (this.state.Ques5Status == true && this.state.Ques5 == "") {
                alert("Please enter all mandatory field");
            }
            else if (this.state.Ques6Status == true && this.state.Ques6 == "") {
                alert("Please enter all mandatory field");
            }
            else if (this.state.AttestedQues1 == false || this.state.AttestedQues2 == false || this.state.AttestedQues3 == false || this.state.AttestedQues4 == false ||
                this.state.AttestedQues5 == false || this.state.AttestedQues6 == false || this.state.AttestedQues7 == false || this.state.AttestedQues8 == false ||
                this.state.AttestedQues9 == false) {
                alert("Please select all Attestation")
            }
            // if (this.state.firstName == "" || this.state.PaymentMethodCode == "" || this.state.nameOnAccount == "" || this.state.accountNumber == ""
            //   || this.state.countryOfOperation == ""
            //   || this.state.routingNumber == "" || this.state.serviceClassCode == ""
            // ) {
            //   alert("Please enter all mandatory field(s)")
            // }
            else if (this.state.iAgree == false) {
                alert("Please agree the above provide information")
            }
            else if (this.state.Signature == "") {
                alert("Please provide your signatue")
            }
            else if (this.state.SubmitDate == null) {
                alert("Please provide date")
            }
            // else if (this.state.toAccountHandlerEmails == "") {
            //     alert("Account Handler cannot be empty");

            // }
            // if (this.state.firstName == "" || this.state.PaymentMethodCode == "" || this.state.nameOnAccount == "" || this.state.accountNumber == ""
            //   || this.state.countryOfOperation == ""
            //   || this.state.routingNumber == "" || this.state.serviceClassCode == ""
            // ) {
            //   alert("Please enter all mandatory field(s)")
            // }
            // else if (updateData.length == 0) {
            //   alert("Please select any record from Company table")
            // }
            // else if (this.state.instittionPhoneNumber.length < 10 && this.state.instittionPhoneNumber != "" && this.state.instittionPhoneNumber != null) {
            //   alert("Length of Institution Phone Number should be correct")
            // }
            else if (getResults[0] != undefined && itemid != null) {
                {
                    let resultID =
                        await billcomp.updateDataToAddCompanyList(userTitle, this.state.additionalComments, this.state.Ques1, this.state.Ques2,
                            this.state.Ques3, this.state.Ques4Status, this.state.Ques5, this.state.Ques6, this.state.Ques7Status,
                            this.state.Ques1Status, this.state.Ques2Status, this.state.Ques3Status, this.state.Ques5Status,
                            this.state.Ques6Status, this.state.AttestedQues1, this.state.AttestedQues2, this.state.AttestedQues3, this.state.AttestedQues4,
                            this.state.AttestedQues5, this.state.AttestedQues6, this.state.AttestedQues7,
                            this.state.AttestedQues8, this.state.AttestedQues9, this.state.iAgree, this.state.selectedReqDate, this.state.Signature
                            , this.state.SubmitDate, this.state.Comments, NotesFinanceTeamcommentsText, this.state.Office, mainListProps, itemid);
                    if (resultID != "") {
                        this.setState({ isLoadingImage: false });
                        window.location.hash = 'requests/all';
                        // location.href = context.pageContext.web.absoluteUrl;
                    }
                }
            }
        }
        catch (error) {
            alert(error + "error while inserting the item")
        }
    }
    //approve/reject by AP handlers
    // public async UpdateBillDifferToMainList(vrStatus,vrItemHistoryStatus) {
    //     try {
    //         if (mainListProps == "" || mainListProps == undefined) {
    //             alert("Plese provide list name");
    //         }
    //         // else if (this.state.StatusChangeTitleSelectedKey == "" || this.state.StatusChangeTitleSelectedKey == undefined) {
    //         //     alert("Plese select any Status");
    //         // }
    //         else if (getResults[0] != undefined && itemid != null) {
    //             let resultID =""
    //                 // await billcomp.updateDataToclientrefundList(vrStatus,vrItemHistoryStatus,this.state.itemHistorySummary,this.state.savedInEpic,
    //                 //     this.state.StatusChangeSelectedTitle, userTitle, this.state.CurrentLevel, this.state.SalesCommentsSummary, this.state.financeCommentsSummary,
    //                 //     NotesServiceTeamCommentsText, NotesFinanceTeamcommentsText, mainListProps, itemid);
    //             if (resultID != "") {
    //                // alert("Thank you for your response");
    //                 window.location.hash = 'requests/all';
    //                 // location.href = context.pageContext.web.absoluteUrl;
    //             }
    //         }
    //     }
    //     catch (error) {
    //         alert("error while inserting the item")
    //     }
    // }
    //     public getDataFromInvoiceNumber(): Promise<any> {
    //         if (this.state.EpicInvoiceNumber == "") {
    //             alert("Please enter EPIC Invoice Number");
    //         }
    //         else {
    //             this.setState({ isLoadingImage: true });
    //             var myoutput = [];
    //             var apiUrl = "";
    //             // if (this.state.EpicInvoiceNumber == "") {
    //             //     apiUrl = 'https://qa.wsandco.com/WoodruffEpicAPI/GetInvoiceDetails?clientName=' + this.state.Client
    //             // }
    //             // else if (this.state.Client == "") {
    //             //     apiUrl = 'https://qa.wsandco.com/WoodruffEpicAPI/GetInvoiceDetails?invoiceNumber=' + this.state.EpicInvoiceNumber
    //             // }
    //             // else {
    //             //     apiUrl = 'https://qa.wsandco.com/WoodruffEpicAPI/GetInvoiceDetails?invoiceNumber=' + this.state.EpicInvoiceNumber + "&clientName=" + this.state.Client;
    //             // }
    //             apiUrl = 'https://qa.wsandco.com/WoodruffEpicAPI/GetInvoiceDetails?invoiceNumber=' + this.state.EpicInvoiceNumber  

    //             return this.props.context.spHttpClient
    //                 .get(
    //                     apiUrl,
    //                     //'https://qa.wsandco.com/WoodruffEpicAPI/GetInvoiceDetails?invoiceNumber=' + this.state.EpicInvoiceNumber+"&clientName="+this.state.Client,
    //                     //'https://qa.wsandco.com/WSUtilityService/api/Companies/list?searchString=' + this.state.accountName,
    //                     SPHttpClient.configurations.v1
    //                 )
    //                 .then((response: HttpClientResponse) => {
    //                     return response.json();
    //                 })
    //                 .then(jsonResponse => {
    //                     if (jsonResponse.Data.length > 0) {
    //                         this.setState({ showGrid: true });
    //                        // if (this.state.EpicInvoiceNumber != "") {
    //                             this.setState({
    //                                 Client:jsonResponse.Data[0].Lookupcode + "-" + jsonResponse.Data[0].ClientName, Carrier: jsonResponse.Data[0].CarrierName,
    //                                 policyNumber: jsonResponse.Data[0].PolicyNumber
    //                             });
    //                        // }
    //                         if (jsonResponse.Data[0].AHDetails.length > 0) {
    //                             this.setState({ FilteredListItems: jsonResponse.Data[0].AHDetails });
    //                         }
    //                     }
    //                     else {
    //                         this.setState({ optionClient: [], selectedvalue: null, isDataSelected: false });
    //                     }
    //                     this.setState({ isLoadingImage: false });
    //                     this.setState({ isDataReceived: true });
    //                     return jsonResponse;
    //                 }) as Promise<any>;
    //         }

    //     }
    //     public getDataFromInvoiceNumberClient(clientName): Promise<any> {

    //         this.setState({ isLoadingImage: true });
    //         var myoutput = [];
    //         var apiUrl = "";           
    //              apiUrl = 'https://qa.wsandco.com/WoodruffEpicAPI/GetInvoiceDetails?clientName=' + clientName.trim()
    //         return this.props.context.httpClient
    //             .get(
    //                 apiUrl,
    //                 HttpClient.configurations.v1
    //             )
    //             .then((response:HttpClientResponse) => {
    //                 return response.json();
    //             })
    //             .then(jsonResponse => {
    //                 if (jsonResponse.Data.length > 0) {
    //                     this.setState({ showGrid: true });
    //                         // this.setState({
    //                         //     Client:jsonResponse.Data[0].Lookupcode +"-" + jsonResponse.Data[0].ClientName, Carrier: jsonResponse.Data[0].CarrierName,
    //                         //     policyNumber: jsonResponse.Data[0].PolicyNumber

    //                         // });
    //                     if (jsonResponse.Data[0].AHDetails.length > 0) {
    //                         this.setState({ FilteredListItems: jsonResponse.Data[0].AHDetails });
    //                     }
    //                 }
    //                 else {
    //                     this.setState({ optionClient: [], selectedvalue: null, isDataSelected: false });
    //                 }
    //                 this.setState({ isLoadingImage: false });
    //                 this.setState({ isDataReceived: true });
    //                 return jsonResponse;
    //             }) as Promise<any>;
    // }
    //     public getDataFromInvoiceNumberClientName(val): Promise<any> {
    //         this.setState({ isLoadingImage: true });
    //         var myoutput: any = [];
    //         let listofCountries: Ioption[] = [];
    //         var apiUrl = "";
    //         apiUrl = 'https://qa.wsandco.com/WoodruffEpicAPI/GetEntityList?entityKey=cust&entityName=' + val
    //         return this.props.context.spHttpClient
    //             .get(
    //                 apiUrl,
    //                 //'https://qa.wsandco.com/WoodruffEpicAPI/GetInvoiceDetails?invoiceNumber=' + this.state.EpicInvoiceNumber+"&clientName="+this.state.Client,
    //                 //'https://qa.wsandco.com/WSUtilityService/api/Companies/list?searchString=' + this.state.accountName,
    //                 SPHttpClient.configurations.v1
    //             )
    //             .then((response: HttpClientResponse) => {
    //                 return response.json();
    //             })
    //             .then(jsonResponse => {
    //                 if (jsonResponse.Data.length > 0) {
    //                     jsonResponse.Data.forEach(element => {
    //                         listofCountries.push({ value: element.LookupCode + " - " + element.Description, label: element.LookupCode + " - " + element.Description });
    //                     });
    //                     this.setState({ optionClient: listofCountries, isDataSelected: true });
    //                     this.setState({ isDataReceived: true });
    //                 }
    //                 else {
    //                     this.setState({ optionClient: [], selectedvalue: null, isDataSelected: false });
    //                     this.setState({ isDataReceived: false });
    //                 }
    //                 this.setState({ isLoadingImage: false });

    //                 return jsonResponse;
    //             }) as Promise<any>;
    //     }
    //     private handleChange = (selectedvalue) => {

    //         var vrSelectedvalLength = selectedvalue.label.split("-").length
    //         var vrSelectedval=selectedvalue.label.split("-")[vrSelectedvalLength-1];

    //         this.getDataFromInvoiceNumberClient(vrSelectedval);

    //         this.setState({ selectedvalue, Client: selectedvalue.label, isDataSelected: false });

    //     }
    public _onCancel() {
        window.location.hash = 'requests/all';
    }
    // private async handleSaveUpdate() {
    //     //const { requestorSection } = this.state;
    //     if (mainListProps == "" || mainListProps == undefined) {
    //         alert("Plese provide list name");
    //     }
    //     // else if (this.state.toAccountHandlerEmails == "") {
    //     //     alert("Account Handler cannot be empty");

    //     // }
    //     this.setState({ isLoadingImage: true });
    //     //FinanceApprover
    //     let requestDetail = {};
    //     var vrFinanceTeamCommentsText = NotesFinanceTeamcommentsText == undefined || NotesFinanceTeamcommentsText == null ? "" : NotesFinanceTeamcommentsText;
    //     var finalFinanceComments = "";
    //     if (vrFinanceTeamCommentsText != "") {
    //         if (this.state.financeCommentsSummary == null) {
    //             this.setState({financeCommentsSummary: ""});
    //         }
    //         finalFinanceComments = this.state.financeCommentsSummary + "<br/>" + new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() + " - " + userTitle + "<br/>" + vrFinanceTeamCommentsText;
    //     }
    //     else
    //         finalFinanceComments = this.state.financeCommentsSummary;
    //     //requestDetail["TriggerFlow"] = false;
    //     let requestRes = await billcomp.updateSaveasDraftEdit(this.props.RequestListID,userTitle,this.state.itemHistorySummary,this.state.savedInEpic, finalFinanceComments, Number(this.props.ID));
    //     if (requestRes!="") {
    //         this.setState({ isLoadingImage: false });
    //         //  NotificationManager.success('Request successfully updated.', 'Successful!', 5000);
    //         window.location.hash = 'requests/all';
    //     }
    // }
    //when request is withdrawn from requestor
    // private async btnSUbmitWithdrawn() {
    //     if (getResults[0] != undefined && itemid != null) {
    //         await billcomp.updateStatusWithDrawnToAddCompanyList("Withdrawn", userTitle,this.state.itemHistorySummary, this.state.SalesCommentsSummary, this.state.financeCommentsSummary,
    //             NotesServiceTeamCommentsText, NotesFinanceTeamcommentsText, mainListProps, itemid);
    //        // alert("Thank you your response is withdrawn");
    //        window.location.hash = 'requests/all';
    //     }
    // }

    private collapseItemHistoryHandler() {
        this.setState({
            isCollapse: !this.state.isCollapse
        });
    }
    private collapseFinanceHandler() {
        this.setState({
            isFinanceCollapse: !this.state.isFinanceCollapse
        });
    }

    private NotesFinanceTeamRichTxtChangedNew(e) {
        NotesFinanceTeamcommentsText = e.target.value;
    }
    private NotesServiceTeamRichTxtChangedNew(e) {
        NotesServiceTeamCommentsText = e.target.value;
    }
    public render(): React.ReactElement<IEditRequestProps> {
        const rowStyle = (row, rowIndex) => {
            const style = {};
            if (rowIndex % 2 === 0) {
                return { backgroundColor: '#c8e6c9' }
            }
            else {
                style: "green";
            }
            return style;// rowIndex % 2 === 0 ? style: "red" : style "green";
        };
        const value = this.state.selectedvalue;
        let classes: any = useStyles;
        let backDropClasses: any = backdropStyles;
        const ListColumn = [
            {
                dataField: 'Department', text: 'Department'
            },
            {
                dataField: 'AHName', text: 'Account Handler'
            },
        ]
        let attachmentHTML: any[] = [];
        //if (requestorSection && loading && isRequestExists && isAuthorized) {

        return (
            <div className={styles.wsclientrefund}>
                {/* <div className='divHeader'>{this.props.context}</div> */}
                <br />
                <div className={classes.root}>
                    <div className="divclear">
                        <Backdrop className={backDropClasses.backdrop} style={{ zIndex: 9 }} open={this.state.isLoadingImage}>
                            <CircularProgress color="inherit" />
                        </Backdrop>
                    </div>
                    <div className={styles.boxRow}>
                        <div className="divclear" style={{ minHeight: "20px" }}>
                            <div className="" style={{ padding: "6px", paddingLeft: "25px" }}>
                                <Button size="medium" color="default" variant="contained" onClick={(e) => { this._onCancel() }} className={classes.button} style={{ margin: '20px' }}>
                                    <div style={{ marginLeft: '5px' }}>Cancel</div>
                                </Button>
                                {this.state.isCreatedBy && !this.state.isCompleted && <Button size="medium" color="primary" variant="contained" onClick={(e) => { this.UpdateBillDifferToMainList("Approved") }} className={classes.button} style={{}}>
                                    <div>Submit</div>
                                </Button>
                                }

                            </div>
                        </div>
                        {/* <div className="dvConfigData">
                                    <div dangerouslySetInnerHTML={{ __html: Configoutput }}></div>
                                </div> */}
                        <br />
                        <div className="dvFirstHeader" style={{ float: "left", width: "98%" }}>
                            <div className={styles.columnrow} style={{ paddingTop: "10px" }}>
                                <Label className="divMainHeader" >Return to Office Questionnaire</Label>
                            </div>
                            <div className="divclear">
                                <div className={styles.columnrow}>
                                    <Label style={{ float: "left", paddingRight: "10px", width: "135px" }} className="divCommon">Name:</Label>
                                    <div style={{ paddingTop: "7px" }} className="">{this.state.firstName}</div>
                                </div>
                                <div className={styles.columnrow}>
                                    <Label style={{ float: "left", paddingRight: "10px", paddingTop: "8px", width: "135px" }} className="divCommon">Requested Date:<span className="spnRequired">*</span></Label>
                                    <div style={{ paddingTop: "7px", width: "28%" }} >
                                        <DatePicker placeholder="Select a date..."
                                            value={this.state.selectedReqDate}
                                            formatDate={this._onFormatDate}
                                            disabled={this.state.isDataSelected}
                                            onSelectDate={(e) => this.handleChangeRetDate(e)}
                                        /></div>
                                </div>
                                <div className={styles.columnrow}>
                                    <Label style={{ float: "left", paddingRight: "10px", paddingTop: "8px", width: "135px" }} className="divCommon">Office:<span className="spnRequired">*</span> </Label>
                                    <div style={{ paddingTop: "7px", float: "left", width: "40%" }} >
                                        <TextField
                                            id="office"
                                            name="Office"
                                            label="Office"
                                            variant="outlined"
                                            size="small"
                                            required
                                            fullWidth
                                            onChange={(e) => this.handleChangeOffice(e)}
                                            value={this.state.Office}
                                            disabled={this.state.isDataSelected}
                                        /></div>
                                </div>
                            </div>
                            <div className={styles.columnrow}> <TextField
                                id="addComments"
                                name="addComments"
                                label="Additional Comments"
                                variant="outlined"
                                size="small"
                                rows={5}
                                disabled={this.state.isDataSelected}
                                multiline
                                fullWidth
                                onChange={(e) => this.handleChangeadditionalComments(e)}
                                value={this.state.additionalComments}
                            /></div>
                            <div style={{ paddingTop: "5px", clear: "both" }} >
                                <hr className="divHr" />
                            </div>
                            <div className={styles.columnrow} >
                                <div style={{ width: "85%", float: "left" }}><Label className="divCommon">{this.props.Question1Status}</Label></div>
                                <div style={{ width: "15%", float: "left", paddingBottom: "10px" }}>
                                    <Dropdown
                                        placeholder="Select an option"
                                        options={ddlOptions}
                                        selectedKey={this.state.ddlQues1Status}
                                        onChange={this.fnQues1Status.bind(this)}
                                        disabled={this.state.isDataSelected}
                                    />
                                </div>
                                {this.state.isHideQues1Status && <div style={{ width: "20%" }}>
                                    <DatePicker placeholder="Select a date..."
                                        onSelectDate={(e) => this.handleChangeQues1(e)}
                                        value={this.state.Ques1}
                                        formatDate={this._onFormatDate}
                                        disabled={this.state.isDataSelected}

                                    />
                                </div>}
                            </div>
                            <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}><Label className="divCommon">{this.props.Question2Status} <span className="spnRequired">*</span></Label></div>
                                <div style={{ width: "15%", float: "left", paddingBottom: "10px" }}>
                                    <Dropdown
                                        placeholder="Select an option"
                                        options={ddlOptions}
                                        disabled={this.state.isDataSelected}
                                        selectedKey={this.state.ddlQues2Status}
                                        onChange={this.fnQues2Status.bind(this)}
                                    />
                                </div>
                                {this.state.isHideQues2Status && <div > <TextField
                                    id="Ques2"
                                    name="Ques2"
                                    label=""
                                    variant="outlined"
                                    size="small"
                                    rows={5}
                                    multiline
                                    fullWidth
                                    disabled={this.state.isDataSelected}
                                    onChange={(e) => this.handleChangeQues2(e)}
                                    value={this.state.Ques2}
                                /></div>}

                            </div>
                            <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}> <Label className="divCommon">{this.props.Question3Status}<span className="spnRequired">*</span></Label></div>
                                <div style={{ width: "15%", float: "left", paddingBottom: "10px" }}>
                                    <Dropdown
                                        placeholder="Select an option"
                                        options={ddlOptions}
                                        disabled={this.state.isDataSelected}
                                        selectedKey={this.state.ddlQues3Status}
                                        onChange={this.fnQues3Status.bind(this)}
                                    />
                                </div>
                                {this.state.isHideQues3Status && <div >    <TextField
                                    id="Ques3"
                                    name="Ques3"
                                    label=""
                                    variant="outlined"
                                    size="small"
                                    rows={5}
                                    multiline
                                    fullWidth
                                    disabled={this.state.isDataSelected}
                                    onChange={(e) => this.handleChangeQues3(e)}
                                    value={this.state.Ques3}
                                />
                                </div>}
                            </div>
                            <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}> <Label className="divCommon">{this.props.Question4Status}</Label></div>
                                <div style={{ width: "15%", float: "left" }}>
                                    <Dropdown
                                        placeholder="Select an option"
                                        options={ddlOptions}
                                        selectedKey={this.state.ddlQues4Status}
                                        onChange={this.fnQues4Status.bind(this)}
                                        disabled={this.state.isDataSelected}
                                    />
                                </div>
                                {/* <FormControlLabel
                                            control={
                                                <Checkbox id="chkApprove" required checked={this.state.Ques4} onChange={this.handleChangeQues4.bind(this)} name="chkStatus" />}
                                            label=""
                                        /> */}
                            </div>
                            <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}>  <Label className="divCommon">{this.props.Question5Status}<span className="spnRequired">*</span></Label></div>
                                <div style={{ width: "15%", float: "left", paddingBottom: "10px" }}>
                                    <Dropdown
                                        placeholder="Select an option"
                                        options={ddlOptions}
                                        selectedKey={this.state.ddlQues5Status}
                                        onChange={this.fnQues5Status.bind(this)}
                                        disabled={this.state.isDataSelected}
                                    />
                                </div>
                                {this.state.isHideQues5Status && <div>
                                    <TextField
                                        id="Ques5"
                                        name="Ques5"
                                        label=""
                                        variant="outlined"
                                        size="small"
                                        fullWidth
                                        rows={5}
                                        multiline
                                        onChange={(e) => this.handleChangeQues5(e)}
                                        disabled={this.state.isDataSelected}
                                        value={this.state.Ques5}
                                    />
                                </div>}
                            </div>
                            <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}>   <Label className="divCommon">{this.props.Question6Status}<span className="spnRequired">*</span></Label></div>
                                <div style={{ width: "15%", float: "left", paddingBottom: "10px" }}>
                                    <Dropdown
                                        placeholder="Select an option"
                                        options={ddlOptions}
                                        selectedKey={this.state.ddlQues6Status}
                                        onChange={this.fnQues6Status.bind(this)}
                                        disabled={this.state.isDataSelected}
                                    />
                                </div>
                                {this.state.isHideQues6Status && <div>
                                    <TextField
                                        id="Ques6"
                                        name="Ques6"
                                        label=""
                                        variant="outlined"
                                        size="small"
                                        fullWidth
                                        rows={5}
                                        multiline
                                        onChange={(e) => this.handleChangeQues6(e)}
                                        disabled={this.state.isDataSelected}
                                        value={this.state.Ques6}
                                    />
                                </div>}
                            </div>
                            <div className={styles.columnrow}>
                                <div style={{ width: "85%", float: "left" }}><Label className="divCommon">{this.props.Question7Status}</Label></div>
                                <div style={{ width: "15%", float: "left" }}>
                                    <Dropdown
                                        placeholder="Select an option"
                                        options={ddlOptions}
                                        selectedKey={this.state.ddlQues7Status}
                                        onChange={this.fnQues7Status.bind(this)}
                                        disabled={this.state.isDataSelected}
                                    />
                                </div>
                                {/* <FormControlLabel
                                            control={
                                                <Checkbox id="chkApprove" required checked={this.state.Ques7} onChange={this.handleChangeQues7.bind(this)} name="chkStatus" />}
                                            label="Would you be taking public transportation to the office?"
                                        /> */}
                            </div></div>
                        <div className="dvFirstHeader" style={{ float: "left", width: "98%" }}>
                            <div className={styles.columnrow} style={{ paddingTop: "10px" }}>
                                <Label className="divMainHeader">Attestation</Label>
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues1}
                                            onChange={this.handleChangeAttestedQues1.bind(this)}
                                            name="chkStatus" />}
                                    label="I will take my temperature daily before going into the office and will not go to the office if I have a temperature of 100.4 or over"
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues2}
                                            onChange={this.handleChangeAttestedQues2.bind(this)}
                                            name="chkStatus" />}
                                    label="I will not go into the office on any day I have symptoms of COVID-19, regardless of my temperature, or if I know or suspect I may have COVID-19"
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues3}
                                            onChange={this.handleChangeAttestedQues3.bind(this)}
                                            name="chkStatus" />}
                                    label="I will not go into the office on any day a member of my household is sick or has symptoms of COVID-19"
                                />
                            </div>

                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues4}
                                            onChange={this.handleChangeAttestedQues4.bind(this)}
                                            name="chkStatus" />}
                                    label="I will not go into the office if I am awaiting COVID-19 test results, or if I have been advised by a health care provider or local public health department to self-quarantine "
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues5}
                                            onChange={this.handleChangeAttestedQues5.bind(this)}
                                            name="chkStatus" />}
                                    label="I will practice social distancing and maintain at least six feet of distance from others while in the workplace whenever possible"
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues6}
                                            onChange={this.handleChangeAttestedQues6.bind(this)}
                                            name="chkStatus" />}
                                    label="I will wear a mask in the office at all times, except when seated at my desk"
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues7}
                                            onChange={this.handleChangeAttestedQues7.bind(this)}
                                            name="chkStatus" />}
                                    label="I will wear a face covering as required by law, including in any public areas or when social distancing (maintaining at least 6 feet of distance) is not practical or possible"
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues8}
                                            onChange={this.handleChangeAttestedQues8.bind(this)}
                                            name="chkStatus" />}
                                    label="I agree to notify Elyse Douglas or Zac Overbay if I develop symptoms or believe I may have been exposed to COVID at any time after completing this questionnaire"
                                />
                            </div>
                            <div className={styles.columnrow}>
                                <FormControlLabel
                                    control={
                                        <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.AttestedQues9}
                                            onChange={this.handleChangeAttestedQues9.bind(this)}
                                            name="chkStatus" />}
                                    label="I agree to notify Elyse Douglas or Zac Overbay if the answer to any of the questions above change at any time after completing this questionnaire"
                                />
                            </div>


                            {/* {this.state.isOfficeAdmin && this.state.isApproved && <div className={styles.columnrow}>
                                    <TextField
                                            id="badgeID"
                                            name="badgeID"
                                            label="badgeID"
                                            variant="outlined"
                                            size="small"
                                            fullWidth
                                            value={this.state.badgeID}
                                            onChange={(e) => this.badgeIDTxtChangedNew(e)}
                                        />
                                    </div>}
                                    {this.state.isCompleted && <div className={styles.columnrow}>
                                        <TextField
                                            id="badgeID"
                                            name="badgeID"
                                            label="badgeID"
                                            variant="outlined"
                                            size="small"
                                            fullWidth
                                            disabled={this.state.isCompleted}
                                            value={this.state.badgeID}
                                            onChange={(e) => this.badgeIDTxtChangedNew(e)}
                                        />
                                    </div>} */}

                        </div>
                        <div className={styles.columnrow}>
                            <FormControlLabel
                                control={
                                    <Checkbox disabled={this.state.isDataSelected} id="chkApprove" required checked={this.state.iAgree}
                                        onChange={this.handleChangeAgree.bind(this)}
                                        name="chkStatus" />}
                                label="I attest that all the information I have provided above is true and accurate.  Should any of this information change I recognize I am obligated to submit a new questionnaire."
                            />
                        </div>
                        <div className={styles.columnrow}>
                            <div style={{ width: "40%", float: "left" }}>
                                <TextField
                                    id="signa"
                                    name="sign"
                                    label="Signature"
                                    variant="outlined"
                                    size="small"
                                    fullWidth
                                    disabled={this.state.isDataSelected}
                                    onChange={(e) => this.handleChangeSignature(e)}
                                    value={this.state.Signature}
                                /></div>
                            <div style={{ width: "20%", float: "right" }}> <DatePicker placeholder="Select a date..."
                                onSelectDate={(e) => this.handleChangeSubmittedDate(e)}
                                value={this.state.SubmitDate}
                                disabled={this.state.isDataSelected}
                                formatDate={this._onFormatDate}
                            /></div>
                        </div>
                        {this.state.isdataLoaded &&
                            <div className="divclear">
                                <br />
                                <div className={styles.collapsediv}>
                                    <div className={styles.header1} onClick={e => { this.collapseFinanceHandler() }}>
                                        {!this.state.isFinanceCollapse && <Icon iconName="CollapseContent" className={styles.icon} onClick={e => { this.collapseFinanceHandler() }} />}
                                        {this.state.isFinanceCollapse && <Icon iconName="ExploreContent" className={styles.icon} onClick={e => { this.collapseFinanceHandler() }} />}
                                        <span className={styles.details}>Comments </span>
                                    </div>
                                    <div className={!this.state.isFinanceCollapse ? `${styles.contentE} ${styles.content}` : `${styles.contentC} ${styles.content}`}>
                                        <div style={{ width: "100%" }} dangerouslySetInnerHTML={{ __html: outputFinance }}>
                                        </div>
                                    </div>
                                </div>

                                <div className="rchClass">
                                    <TextField
                                        id="Comments"
                                        name="Comments"
                                        label="Comments"
                                        variant="outlined"
                                        size="small"
                                        fullWidth
                                        multiline
                                        rows={5}
                                        onChange={(e) => this.NotesFinanceTeamRichTxtChangedNew(e)}
                                    />
                                </div>
                            </div>

                        }
                        <br />
                    </div>
                    <br />
                    <div className="divclear" style={{ minHeight: "20px" }}>
                        <div className="" style={{ padding: "6px", paddingLeft: "25px" }}>
                            <Button size="medium" color="default" variant="contained" onClick={(e) => { this._onCancel() }} className={classes.button} style={{ margin: '20px' }}>
                                <div style={{ marginLeft: '5px' }}>Cancel</div>
                            </Button>

                            {this.state.isCreatedBy && !this.state.isCompleted && <Button size="medium" color="primary" variant="contained" onClick={(e) => { this.UpdateBillDifferToMainList("Approved") }} className={classes.button} style={{}}>
                                <div>Submit</div>
                            </Button>
                            }

                        </div>
                    </div>
                </div>
            </div>
        );

    }
}

