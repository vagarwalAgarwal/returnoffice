import { WebPartContext } from "@microsoft/sp-webpart-base";
import { IListService } from "../../../services/SPListServices";

export interface IEditRequestProps {
    ID?: number;
    context?: WebPartContext;    
    ListServices?: IListService;
    RequestListID?: string;
    Question1Status:string;
    Question2Status:string;
    Question3Status:string;
    Question4Status:string;
    Question5Status:string;
    Question6Status:string;
    Question7Status:string;
}
