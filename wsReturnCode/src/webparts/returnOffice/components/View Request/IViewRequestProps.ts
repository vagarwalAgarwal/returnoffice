import { WebPartContext } from "@microsoft/sp-webpart-base";
import { IPropertyFieldGroupOrPerson } from '@pnp/spfx-property-controls/lib/PropertyFieldPeoplePicker';

export interface IViewRequestProps {
    context?: WebPartContext;
    RequestListID?:string;  
    ID:string;
    //isARMember?:boolean;
    ARGroupID:IPropertyFieldGroupOrPerson[];
    OfficeGroupID:IPropertyFieldGroupOrPerson[]; 
    Question1Status:string;
    Question2Status:string;
    Question3Status:string;
    Question4Status:string;
    Question5Status:string;
    Question6Status:string;
    Question7Status:string;
}