import * as React from 'react';
import styles from './ReturnOffice.module.scss';
import { IReturnOfficeProps } from './IReturnOfficeProps';
import { escape } from '@microsoft/sp-lodash-subset';
import { Route, Switch, HashRouter } from 'react-router-dom';
import 'react-notifications/lib/notifications.css';
import { HttpClient, HttpClientResponse } from '@microsoft/sp-http';
import { Create, Person } from "@material-ui/icons";
import { Dashboard as DashboardIcon } from '@material-ui/icons';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Placeholder } from "@pnp/spfx-controls-react/lib/Placeholder";
import { Dashboard } from './Dashboard/Dashboard';
import { NewRequest } from './New Request/NewRequest';
import { ViewRequest } from './View Request/ViewRequest';
import { SPHttpClient, SPHttpClientResponse, ISPHttpClientOptions } from '@microsoft/sp-http';
import { EditRequest } from './Edit Request/EditRequest';
import { IUserProfileService, UserProfileService } from "../../services/SPUserProfileServices";

export default class ReturnOffice extends React.Component<IReturnOfficeProps,any, {}> {
  private clientOptions = [];
  private userProfileServices: IUserProfileService;
  constructor(props: IReturnOfficeProps) {
    super(props);
    this.state = {
      currentUser: null,
      loading: true,
      showCreateButton: true,
      isValidUser: true,
      IsRequestor: true,
      isARMember: false

    };
    this.userProfileServices = new UserProfileService(this.props.context);
    this.OnLoad();
  }


  private OnLoad = async () => {
    let currentUserGroups = await this.userProfileServices.GetAllUsersGroupsFromSharePoint();
    let isARapprover = currentUserGroups.filter(item => item.Id == this.props.ARGroupID[0].id).length > 0 ? true : false;

    this.setState({ isARMember: isARapprover });
    
    }

  private CreateNew(this) {
    window.location.hash = '#/request/new';
  }
  
  private Allrequest() {
    window.location.hash = '#/requests/all';
  }
  private Myrequest() {
    window.location.hash = '#/requests/my';
  }
  public render(): React.ReactElement<IReturnOfficeProps> {
    let isDashboardBtnDisabled = (window.location.hash.toLowerCase() == "#/" || window.location.hash.toLowerCase() == "#/requests/all") ? true : false;
    let isMyRequestBtnDisabled = (window.location.hash.toLowerCase() == "#/requests/my") ? true : false;
    let isNewReqBtnDisabled: boolean = false;
    if (this.state.showCreateButton == false) {
      isNewReqBtnDisabled = true;
    }
    else {
      if (window.location.hash.toLowerCase() == "#/request/new") {
        isNewReqBtnDisabled = true;
      }
      else {
        isNewReqBtnDisabled = false;
      }
    }




    return (
      <div>
        <HashRouter>

          <div className={styles.wsBillingDifference}>
            <div className={styles.row}>
              <div>
                <div className={styles.header}>
                {this.props.pageTitle}
                                </div>
                <div className={styles.buttonTop}>
                  <ButtonGroup variant="contained" color="primary" aria-label="contained primary button group" >
                  {this.state.isARMember &&   <Button id="btnTopAllReq" startIcon={<DashboardIcon />}  className={styles.ButtonMain} onClick={this.Allrequest.bind(this)}>ALL REQUESTS</Button>}
                    {!this.state.isARMember && <Button id="btnTopAllReq" startIcon={<DashboardIcon />}  className={styles.ButtonMain} onClick={this.Myrequest.bind(this)}>My REQUESTS</Button>}
                    <Button style={{ display: this.state.showCreateButton ? "inline-flex" : "none" }} id="btnTopNewReq" startIcon={<Create />} className={styles.ButtonMain} onClick={this.CreateNew.bind(this)}>CREATE NEW REQUEST</Button>
                  </ButtonGroup>
                </div>
              </div>
            </div>
          </div>
          {(this.state.loading == true ?
            <div className={styles.container}>
              {this.state.isValidUser ?
                <Switch>
                  <Route path="/" exact={true} component={() =>
                    <Dashboard
                      displayItemsCount={100}
                      RequestListID={this.props.RequestList}
                      listServices={this.props.listServices}
                      CurrentUser={this.props.context.pageContext.legacyPageContext.userId}
                      isARMember = {this.state.isARMember}
                    />}
                  />
                  <Route path="/requests" exact={true} component={() =>
                    <Dashboard
                      displayItemsCount={100}
                      RequestListID={this.props.RequestList}
                      listServices={this.props.listServices}
                      CurrentUser={this.props.context.pageContext.legacyPageContext.userId}
                      isARMember = {this.state.isARMember}
                    />} />
                  <Route path="/requests/all" exact={true} component={() =>
                    <Dashboard
                      displayItemsCount={100}
                      RequestListID={this.props.RequestList}
                      listServices={this.props.listServices}
                      CurrentUser={this.props.context.pageContext.legacyPageContext.userId}
                      isARMember = {this.state.isARMember}
                    />} />
                  <Route path="/requests/my" exact={true} component={() =>
                    <Dashboard
                      displayItemsCount={100}
                      RequestListID={this.props.RequestList}
                      listServices={this.props.listServices}
                      CurrentUser={this.props.context.pageContext.legacyPageContext.userId}
                      isARMember = {this.state.isARMember}
                    />} />
                  {/* <Route path="/:status" exact={true} component={({ match }) => */}
                  <Route path="/requests/all/:type/:status" exact={true} component={({match}) =>
                    <Dashboard
                      displayItemsCount={100}
                      RequestListID={this.props.RequestList}
                      listServices={this.props.listServices}
                      CurrentUser={this.props.context.pageContext.legacyPageContext.userId}
                      isARMember = {this.state.isARMember}
                      filter={match.params.status}
                      type={match.params.type}
                    />} />
                  <Route path="/request/new" exact={true}
                    render={({ match }) => {
                      return this.state.IsRequestor ?
                       <NewRequest
                        RequestListID={this.props.RequestList}
                        context={this.props.context}
                        Question1Status={this.props.Question1Status}
                        Question2Status={this.props.Question2Status}
                        Question3Status={this.props.Question3Status}
                        Question4Status={this.props.Question4Status}
                        Question5Status={this.props.Question5Status}
                        Question6Status={this.props.Question6Status}
                        Question7Status={this.props.Question7Status}             
                      /> : <Placeholder
                        iconName='WarningSolid'
                        iconText='Access Denied'
                        description='You do not have permission to create a new request. Please contact Site Administrator to request access.'>
                        </Placeholder>;
                    }}
                  />
                                
                  <Route
                    exact
                    path="/request/view/:Id"
                    render={({ match }) => {
                      return <ViewRequest
                        RequestListID={this.props.RequestList} 
                        ID={match.params.Id}                        
                        context={this.props.context}   
                        ARGroupID = {this.props.ARGroupID}
                        OfficeGroupID={this.props.OfficeGroupID}
                        Question1Status={this.props.Question1Status}
                        Question2Status={this.props.Question2Status}
                        Question3Status={this.props.Question3Status}
                        Question4Status={this.props.Question4Status}
                        Question5Status={this.props.Question5Status}
                        Question6Status={this.props.Question6Status}
                        Question7Status={this.props.Question7Status}           
                      />;
                    }}
                  />
                    
                  <Route
                    exact
                    path="/request/edit/:Id"
                    render={({ match }) => {
                      return this.state.IsRequestor ? <EditRequest
                        RequestListID={this.props.RequestList}                        
                        ID={match.params.Id}
                        ListServices={this.props.listServices}                        
                        context={this.props.context} 
                        Question1Status={this.props.Question1Status}
                        Question2Status={this.props.Question2Status}
                        Question3Status={this.props.Question3Status}
                        Question4Status={this.props.Question4Status}
                        Question5Status={this.props.Question5Status}
                        Question6Status={this.props.Question6Status}
                        Question7Status={this.props.Question7Status}                       
                        //IsRequestor={this.state.IsRequestor}                       
                        //CPDSPGroupId={this.props.CPDSPGroupId}                        
                      ></EditRequest> : <Placeholder iconName='WarningSolid'
                        iconText='Access Denied'
                        description='You do not authorized to edit the request.'
                      ></Placeholder>;
                    }}
                  /> 
                  
                  <Route path="*">
                    <Placeholder
                      iconName='WarningSolid'
                      iconText='Invalid URL'
                      description='Please check the URL.'>
                    </Placeholder>
                  </Route>
                </Switch>
                :
                <div>
                  <div className={styles.row}>
                    <Placeholder iconName='WarningSolid'
                      iconText='Access Denied'
                      description='You do not have permission to access this page. Please contact Site Administrator to request access.'
                    />
                  </div>
                </div>}

            </div>
            : <div>
              <LinearProgress color="primary" />
            </div>)}

        </HashRouter>
      </div>
    );
  }
}
