import { WebPartContext } from "@microsoft/sp-webpart-base";
import { IListService } from "../../services/SPListServices";
import { IPropertyFieldGroupOrPerson } from '@pnp/spfx-property-controls/lib/PropertyFieldPeoplePicker';

export interface IReturnOfficeProps {
  description: string;
  RequestList:string;    
  context:WebPartContext;
  listServices:IListService;
  ARGroupID:IPropertyFieldGroupOrPerson[];
  OfficeGroupID:IPropertyFieldGroupOrPerson[];
  Question1Status:string;
  Question2Status:string;
  Question3Status:string;
  Question4Status:string;
  Question5Status:string;
  Question6Status:string;
  Question7Status:string;
  pageTitle:string;
}
