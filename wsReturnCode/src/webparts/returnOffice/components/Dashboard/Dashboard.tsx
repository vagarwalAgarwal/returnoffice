
import * as React from 'react';
import { sp } from "@pnp/sp";
import { Link } from "react-router-dom";
import { IDashboardProps } from "./IDashboardProps";
import MUIDataTable from "mui-datatables";
import styles from './Dashboard.module.scss';
import * as moment from 'moment';
import IconButton from '@material-ui/core/IconButton';
import DateRangeIcon from '@material-ui/icons/DateRange';
import Tooltip from '@material-ui/core/Tooltip';
import { DefaultButton, PrimaryButton } from 'office-ui-fabric-react';
import { DatePicker, DayOfWeek, Dialog, DialogType, DialogFooter, Label } from 'office-ui-fabric-react';
import { mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
import CircularProgress from '@material-ui/core/CircularProgress';

const today: Date = new Date(Date.now());
const controlClass = mergeStyleSets({
    control: {
        margin: '0 0 15px 0',
        maxWidth: '300px',
    },
});

const onFormatDate = (date: Date): string => {
    return (date.getMonth() + 1) + '/' + date.getDate() + '/' + (date.getFullYear() % 100);
};

export class Dashboard extends React.Component<IDashboardProps, any> {
    private today = new Date(Date.now());
    private gridColumns = [];
    public constructor(props: IDashboardProps) {
        super(props);

        this.state = {
            items: [],
            Showfilter: false,
            loading: true,
            currentPage: 1,
            searchStartDate: "",
            searchEndDate: "",
            searchRequestitems: [],
            hideDateRangeModal: true,
            dateErrorMsg: ""
        };
        this.gridColumns = [
            {
                name: "Id",
                label: "Req#",
                download: false,
                options: {
                    filter: false,
                    sort: true,                    
                }
            },
            {
                name: "Status",
                label: "Status",
                download: false,
                options: {
                    filter: true,
                    //display: false,
                    sort: true,
                    customBodyRender: (value, t, f, v) => {
                        let navLink = "/request/View/" + t.rowData[0];
                         return (
                            <Link to={navLink} activeClassName="current">
                                {value}
                            </Link>

                        );
                    }
                }
            },
            {
                name: "Office",
                label: "Office",
                download: true,
                options: {
                    filter: true,
                    display: true,
                    sort: true
                }
            }, 
            {
                name: "Action",
                label: "Action #",
                download: false,

                options: {
                    filter: false,
                    display: false,
                    sort: true
                }
            },           
            {
                name: "Question1Status",
                label: "Household tested positive ",
                download: false,
                options: {
                    filter: false,
                    sort: true,
                }
            },
            {
                name: "Question3Status",
                label: "CDC identified high-risk facility",
                download: false,
                options: {
                    filter: false,
                    sort: true
                }
            },
            {
                name: "Question5Status",
                label: "Trip outside your home",
                download: false,
                options: {
                    filter: false,
                    sort: true
                }
            },
            {
                name: "Question6Status",
                label: "Attended any gathering larger ",
                download: false,
                options: {
                    filter: false,
                    sort: true
                }
            },
            {
                name: "publictransportationoffice",
                label: "Public transportation",
                download: false,
                options: {
                    filter: false,
                    sort: true,
                }
            },
            {
                name: "ReturnDate",
                label: "Requested Date",
                download: true,
                options: {
                    filter: true,
                    sort: true
                }
            },
            {
                name: "Author",
                label: "Requestor",
                download: true,
                options: {
                    filter: true,
                    sort: true
                }
            },
            {
                name: "Created",
                label: "Submitted Date",
                download: true,
                options: {
                    filter: true,
                    sort: true
                }
            },
            // {
            //     name: "IssueType",
            //     label: "Issue Type",

            //     download: false,
            //     options: {
            //         filter: true,
            //         sort: true,
            //         filterList: this.props.type == undefined ? null : [this.props.type],

            //     }
            // },
            // {
            //     name: "AccountHandler",
            //     label: "Account Handler",
            //     download: false,
            //     options: {
            //         filter: true,
            //         sort: true
            //     }
            // },
            // {
            //     name: "AssignedTo",
            //     label: "Assigned To",
            //     download: false,
            //     options: {
            //         filter: true,
            //         sort: true,
            //     }
            // },


        ];

        this.getListItems(this);
    }
    private handleClick = () => {
        this.setState({
            hideDateRangeModal: false
        });
    }
    public render(): React.ReactElement<IDashboardProps> {
        const optionsnew = {
            filterType: 'multiselect',
            selectableRows: false,
            print: false,
            viewColumns: false,
            download: true,
            rowsPerPage: 20,
            sortOrder: {
                name: 'Id',
                direction: 'desc'
            },
            rowsPerPageOptions: [20, 40, 60, 80, 200],
            textLabels: {
                body: {
                    noMatch: this.state.loading ? (<CircularProgress />) : (<div>No Requests Found</div>)
                }
            },
            onFilterChange: (changedColumn, filterList) => {
                console.log(changedColumn, filterList);
            },
            customToolbar: () => {
                return (

                    <Tooltip title={"Date Range Search"}>
                        <IconButton onClick={this.handleClick}>
                            <DateRangeIcon />
                        </IconButton>
                    </Tooltip>

                );
            },
            downloadOptions: { filename: 'ChangeControl.csv', separator: ',' },

            onDownload: (buildHead, buildBody, columns1, data) => {
                if (true) {
                    var csv = `${buildHead(columns1)}${buildBody(data)}`.trim();
                    this.downloadCSV(csv, 'JVRequests.csv');
                }

                return false;
            }

        };
        const { searchRequestitems, hideDateRangeModal, searchStartDate, searchEndDate, dateErrorMsg } = this.state;
        return (

            <div className={styles.dashboard}>
                <MUIDataTable
                    data={searchRequestitems}
                    columns={this.gridColumns}
                    options={optionsnew}
                />

            </div>
        );
    }
    private _getItemsByListId = async (listId: string): Promise<any> => {
        let _listItems: any = null;
        let loginUserID = this.props.CurrentUser
        let vrFilter = "";
        if (this.props.isARMember == false) {
            vrFilter = "AuthorId eq " + loginUserID;
        }

        //vrFilter="AuthorId eq '" + id + "'"
        try {
            _listItems = await sp.web.lists.getById(listId).items
                .select("*,Author/ID,Author/Title")
                .expand("Author")
                .filter(vrFilter)
                .top(5000)
                .orderBy("Created", false)
                .get()

        }
        catch (err) {
            console.log(err);
            return null;
        }
        return _listItems;
    }
    // Get List items by passing List Id
    // public GetItemsByListId = async (listId: string, selectFields: string, expand: string, orderBy: string, orderByDir: boolean, top?: number): Promise<any> => {
    //    return await this._getItemsByListId(listId, selectFields, expand, orderBy, orderByDir, top);
    // }
    private getListItems = async (myProps: any): Promise<void> => {
        let listItems: any = [];
        //let expand = "IssueType,AccountHandler";
        try {
            if (myProps.props.listServices && myProps.props.RequestListID) {
                listItems = await this._getItemsByListId(
                    this.props.RequestListID
                );
                if (listItems) {
                    if (listItems.length > 0) {
                        listItems.forEach(element => {
                            element.Action = "View";

                            if (element.Created) {
                                element.Created = moment(element.Created).format("MM/DD/YYYY");
                            }
                            if (element.ReturnDate) {
                                element.ReturnDate = moment(element.ReturnDate).format("MM/DD/YYYY");
                            }
                            if (element.householdpositiveCOVID19) {
                                element.householdpositiveCOVID19 = moment(element.householdpositiveCOVID19).format("MM/DD/YYYY");
                            }
                            if (element.Author) {
                                element.Author = element.Author.Title;
                            }
                            if (element.Question1Status==true) {
                                element.Question1Status = "Yes";
                            }
                           else {
                                element.Question1Status = "No";
                            }
                            if (element.Question3Status==true) {
                                element.Question3Status = "Yes";
                            }
                           else {
                                element.Question3Status = "No";
                            } 
                            if (element.Question5Status==true) {
                                element.Question5Status = "Yes";
                            }
                           else {
                                element.Question5Status = "No";
                            }
                            if (element.Question6Status==true) {
                                element.Question6Status = "Yes";
                            }
                           else {
                                element.Question6Status = "No";
                            }
                            if (element.publictransportationoffice==true) {
                                element.publictransportationoffice = "Yes";
                            }
                           else {
                                element.publictransportationoffice = "No";
                            }
                            // if (element.AccountHandler) {
                            //     var elmAccountHandler="";
                            //     for(var i=0;i<element.AccountHandler.length;i++)
                            //     {
                            //         if(i==0)
                            //             elmAccountHandler=element.AccountHandler[i].Title;
                            //          else
                            //             elmAccountHandler+=", "+element.AccountHandler[i].Title;                                    
                            //     }
                            //     element.AccountHandler = elmAccountHandler;
                            // }

                        });
                    }
                    this.setState({
                        items: listItems,
                        loading: false,
                        searchRequestitems: listItems
                    });
                }


            }
        } catch (err) {
            alert("Error getting in List Iteam " + err);
        }
        return listItems;
    }

    private buildCSV(columns, data, options) {
        var replaceDoubleQuoteInString = columnData =>
            typeof columnData === "string"
                ? columnData.replace(/\"/g, '""')
                : columnData;

        var buildHead = columns1 => {
            return (
                columns1
                    .reduce(
                        (soFar, column) =>
                            column.download
                                ? soFar +
                                '"' +
                                this.escapeDangerousCSVCharacters(
                                    replaceDoubleQuoteInString(column.label || column.name)
                                ) +
                                '"' +
                                options.downloadOptions.separator
                                : soFar,
                        ""
                    )
                    .slice(0, -1) + "\r\n"
            );
        };
        var CSVHead = buildHead(columns);

        const buildBody = data1 => {
            return data1
                .reduce(
                    (soFar, row) =>
                        soFar +
                        '"' +
                        row.data
                            .filter((_, index) => columns[index].download)
                            .map(columnData => replaceDoubleQuoteInString(columnData))
                            .join('"' + options.downloadOptions.separator + '"') +
                        '"\r\n',
                    []
                )
                .trim();
        };
        var CSVBody = buildBody(data);

        var csv = options.onDownload
            ? options.onDownload(buildHead, buildBody, columns, data)
            : `${CSVHead}${CSVBody}`.trim();

        return csv;
    }

    private downloadCSV(csv, filename) {
        const blob = new Blob([csv], { type: "text/csv" });

        /* taken from react-csv */
        if (navigator && navigator.msSaveOrOpenBlob) {
            navigator.msSaveOrOpenBlob(blob, filename);
        } else {
            const dataURI = `data:text/csv;charset=utf-8,${csv}`;

            const URL = window.URL;
            const downloadURI =
                typeof URL.createObjectURL === "undefined"
                    ? dataURI
                    : URL.createObjectURL(blob);

            let link = document.createElement("a");
            link.setAttribute("href", downloadURI);
            link.setAttribute("download", filename);
            link.dataset.interception = "off";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }

    private createCSVDownload(columns, data, options) {
        const csv = this.buildCSV(columns, data, options);

        if (options.onDownload && csv === false) {
            return;
        }

        this.downloadCSV(csv, options.downloadOptions.filename);
    }

    private escapeDangerousCSVCharacters(data) {
        if (typeof data === "string") {
            // Places single quote before the appearance of dangerous characters if they
            // are the first in the data string.
            return data.replace(/^\+|^\-|^\=|^\@/g, "'$&");
        }

        return data;
    }

}
