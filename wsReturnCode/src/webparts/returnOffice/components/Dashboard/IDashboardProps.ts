import { IListService } from "../../../services/SPListServices";

import { IPropertyFieldGroupOrPerson } from '@pnp/spfx-property-controls/lib/PropertyFieldPeoplePicker';
import { WebPartContext } from "@microsoft/sp-webpart-base";

export interface IDashboardProps {
    RequestListID: string;
    listServices: IListService;
    displayItemsCount: number;
    CurrentUser?: any;
    filter?:string;
    type?:string;
   isARMember?:boolean;
    
}
