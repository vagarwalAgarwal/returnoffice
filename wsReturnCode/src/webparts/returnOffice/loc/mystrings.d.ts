declare interface IReturnOfficeWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
}

declare module 'ReturnOfficeWebPartStrings' {
  const strings: IReturnOfficeWebPartStrings;
  export = strings;
}
