
import * as React from "react";
import { default as pnp, ItemAddResult } from "sp-pnp-js";
import ListName from "../Common/Constants";
import { Dropdown, DropdownMenuItemType, IDropdownStyles, IDropdownOption } from 'office-ui-fabric-react/lib/Dropdown';
import ListNames from "../Common/Constants";


let surveyConfigColl: any[] = [];
let UserDetails: any;
let LookupCode: IDropdownOption[] = [];
let type: IDropdownOption[] = [];
let EOGroupInsCo: IDropdownOption[] = [];
let Domicile: IDropdownOption[] = [];
let managerName: string;
let statusOps: IDropdownOption[] = [];
let issueTypeOps: IDropdownOption[] = [];

export class BillingCompany {
//*  Saving Data to Work Tool Parts Quote List *//
public async saveDataToAddCompanyList(userTitle,AdditionalComm,Ques1, Ques2,  Ques3, Ques4, Ques5,
  Ques6, Ques7,Ques1Status,Ques2Status,Ques3Status,Ques5Status,
  Ques6Status, AttestedQues1, AttestedQues2, AttestedQues3, AttestedQues4,
  AttestedQues5,AttestedQues6,AttestedQues7, AttestedQues8,AttestedQues9,iagreeInfo,vrReturnDate,vrSignature
  ,SubmitDate,vrOffice,MainList) {
   
  let ChildID = "";
    try{
  await
    pnp.sp.web.lists.getById(MainList).items.add({
     Title: userTitle,
     Office:vrOffice,
     AdditionalComments:AdditionalComm,
     householdpositiveCOVID19: Ques1 != null ? new Date(Ques1) : null,
     householdpositiveexhibitsymptom: Ques2,
     visitedCDCidentifiedriskfacility: Ques3,
     householdtraveledbyplanetrain:Ques4,
     householdtripoutsidehome: Ques5,
     householdattendlarger15people: Ques6,
     publictransportationoffice: Ques7,

     taketemparaturedaily: AttestedQues1,
       notgoofficehavesymptomsCOVID19: AttestedQues2,
       notgoofficehouseholdsick:AttestedQues3,
       notgoofficeCOVID19testresults:AttestedQues4,
       practicesocialdistance6feetother: AttestedQues5,
       wearmaskofficeseateddesk:AttestedQues6,
       facecoveringsocialdistancing:AttestedQues7,
       notifyElyseZacsymptomcovid: AttestedQues8,
       notifyElyseZacquestionchangetime:AttestedQues9,
       iAgree:iagreeInfo,
       ReturnDate:vrReturnDate,
       Question1Status:Ques1Status,
       Question2Status:Ques2Status,
       Question3Status:Ques3Status,
       Question5Status:Ques5Status,
       Question6Status:Ques6Status,
       date: SubmitDate != null ? new Date(SubmitDate) : null,
       Signature:vrSignature,
       Status:"Submitted"
       
     }).then((item: ItemAddResult) => {
      ChildID = item.data.ID;
    });
  }
  catch(error)
  {
    alert(error + "Error while inserting the item")
  }
  return ChildID;
}

//*  update Status to Billing Finance List *//
public async updateDataToclientrefundList(vrCurrentStatusTitle,userTitle,vrbadgeID, NotesFinanceTeamcommentsText,previousFinanceComments,MainList,itemid) {
   
  var finalFinanceComments ="";
  var vrFinanceTeamCommentsText = NotesFinanceTeamcommentsText == undefined || NotesFinanceTeamcommentsText == null ? "" : NotesFinanceTeamcommentsText;
  if(vrFinanceTeamCommentsText!="")
  {
    finalFinanceComments = previousFinanceComments+"<br/>" +  new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() +" - "+userTitle +  "<br/>" + vrFinanceTeamCommentsText+  "<br/>";
  }
  else
  {
    finalFinanceComments = previousFinanceComments;
  }
  
  finalFinanceComments=finalFinanceComments.replace("null","");

  if (vrCurrentStatusTitle == "BadgeID") {
    vrCurrentStatusTitle = "Completed"
  }
  let ChildID;
  await
    pnp.sp.web.lists.getById(MainList).items.getById(itemid).update({
      //Title: firstName,
      Status: vrCurrentStatusTitle,     
      Comments: finalFinanceComments==null?"":finalFinanceComments,
      BadgeID:vrbadgeID

    }).then((item: ItemAddResult) => {
      ChildID = item.data.ID;
    });
  return ChildID;
}
//*  update Data to Billing Finance List *//
public async updateDataToAddCompanyList(userTitle,AdditionalComm,Ques1, Ques2,  Ques3, Ques4, Ques5,
  Ques6, Ques7,Ques1Status,Ques2Status,Ques3Status,Ques5Status,
  Ques6Status, AttestedQues1, AttestedQues2, AttestedQues3, AttestedQues4,
  AttestedQues5,AttestedQues6,AttestedQues7, AttestedQues8,AttestedQues9,iagreeInfo,vrReturnDate,vrSignature
  ,SubmitDate,previousFinanceComments,NotesFinanceTeamcommentsText,vroffice,MainList,itemid) {
    let ChildID;
    var finalFinanceComments ="";
  var vrFinanceTeamCommentsText = NotesFinanceTeamcommentsText == undefined || NotesFinanceTeamcommentsText == null ? "" : NotesFinanceTeamcommentsText;
  if(vrFinanceTeamCommentsText!="")
  {
    finalFinanceComments = previousFinanceComments+"<br/>" +  new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() +" - "+userTitle +  "<br/>" + vrFinanceTeamCommentsText+  "<br/>";
  }
  else
  {
    finalFinanceComments = previousFinanceComments;
  }
  
  finalFinanceComments=finalFinanceComments.replace("null","");

  // if (Amount.indexOf('.') == -1 && Amount!="") {
  //   Amount = Amount + ".00";
  // }
  await
  pnp.sp.web.lists.getById(MainList).items.getById(itemid).update({ 
      //Title: firstName, 
     // Title: userTitle,
     Office:vroffice,
     AdditionalComments:AdditionalComm,
     householdpositiveCOVID19: Ques1 != null ? new Date(Ques1) : null,
     householdpositiveexhibitsymptom: Ques2,
     visitedCDCidentifiedriskfacility: Ques3,
     householdtraveledbyplanetrain:Ques4,
     householdtripoutsidehome: Ques5, 
     householdattendlarger15people: Ques6,
     publictransportationoffice: Ques7,

     taketemparaturedaily: AttestedQues1,
       notgoofficehavesymptomsCOVID19: AttestedQues2,
       notgoofficehouseholdsick:AttestedQues3,
       notgoofficeCOVID19testresults:AttestedQues4,
       practicesocialdistance6feetother: AttestedQues5,
       wearmaskofficeseateddesk:AttestedQues6,
       facecoveringsocialdistancing:AttestedQues7,
       notifyElyseZacsymptomcovid: AttestedQues8,
       notifyElyseZacquestionchangetime:AttestedQues9,
       iAgree:iagreeInfo,
       ReturnDate:vrReturnDate,
       Question1Status:Ques1Status,
       Question2Status:Ques2Status,
       Question3Status:Ques3Status,
       Question5Status:Ques5Status,
       Question6Status:Ques6Status,
       date: SubmitDate != null ? new Date(SubmitDate) : null,
       Signature:vrSignature,
       Comments:finalFinanceComments,
       Status:"Submitted"

     }).then((item: ItemAddResult) => {
      ChildID = item.data.ID;
    });
  return ChildID;
}
public async updateStatusWithDrawnToAddCompanyList(statusData,userTitle,vrPreviousHistory,NotesServiceTeamCommentsText, NotesFinanceTeamcommentsText,previousSalesComments,previousFinanceComments,MainList, itemid) {
  let ChildID;
  var vrNotesServiceTeamCommentsText = NotesServiceTeamCommentsText == undefined || NotesServiceTeamCommentsText == null ? "" : NotesServiceTeamCommentsText;
  var finalSalesComments ="";
  var itemHistory=vrPreviousHistory+"<br/>"+new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() + " - " + userTitle + " - " + " Request Withdrawn, Withdraw <br />";

  if(vrNotesServiceTeamCommentsText!="")
  {     if(previousSalesComments==null)
              previousSalesComments=""
        finalSalesComments =previousSalesComments+"<br/>" + new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() +" - "+userTitle +  "<br/>" + vrNotesServiceTeamCommentsText;
  }
  else
    finalSalesComments =previousSalesComments;
  var vrFinanceTeamCommentsText = NotesFinanceTeamcommentsText == undefined || NotesFinanceTeamcommentsText == null ? "" : NotesFinanceTeamcommentsText;
  var finalFinanceComments ="";
  if(vrFinanceTeamCommentsText!="")
  {
  if(previousFinanceComments==null)
          previousFinanceComments =""
    finalFinanceComments = previousFinanceComments+"<br/>" +  new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() +" - "+userTitle +  "<br/>" + vrFinanceTeamCommentsText;
  }
  else
    finalFinanceComments = previousFinanceComments;
  // var vrNotesServiceTeamCommentsText = NotesServiceTeamCommentsText == undefined || NotesServiceTeamCommentsText == null ? "" : NotesServiceTeamCommentsText;
  // var finalSalesComments =previousSalesComments+"<br/>" + new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() +" - "+userTitle +  "<br/>" + vrNotesServiceTeamCommentsText+  "<br/>";

  // var vrFinanceTeamCommentsText = NotesFinanceTeamcommentsText == undefined || NotesFinanceTeamcommentsText == null ? "" : NotesFinanceTeamcommentsText;
  // var finalFinanceComments = previousFinanceComments+"<br/>" +  new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() +" - "+userTitle +  "<br/>" + vrFinanceTeamCommentsText+  "<br/>";
  try{  
  await
    pnp.sp.web.lists.getById(MainList).items.getById(itemid).update({
      CurrentStatus: statusData,
     NotesFromFinanceTeam: finalFinanceComments,
     NotesFromSalesTeam: finalSalesComments,
       CurrentLevel: statusData,
       TriggerFlow: true,
       ItemHistory:itemHistory

    }).then((item: ItemAddResult) => {
      ChildID = statusData;
    });
  }
  catch (error) {
    alert("updateStatusToAddCompanyList " + error)
  }
  return ChildID;
}
public async updateSaveasDraft(MainList,userTitle,previousHistory,finalFinanceComments,itemid)
{
  let ChildID;
  var itemHistory=previousHistory + "<br/>" + new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() + " - " + userTitle + " - " + " Request, Saved as Draft <br />";

  await
    pnp.sp.web.lists.getById(MainList).items.getById(itemid).update({
      //Title: firstName,
      TriggerFlow: false,        
      NotesFromFinanceTeam: finalFinanceComments==null?"":finalFinanceComments,
      ItemHistory:itemHistory,
     // NotesFromSalesTeam: finalSalesComments==null?"":finalSalesComments

    }).then((item: ItemAddResult) => {
      ChildID = item.data.ID;
    });
  return ChildID;
}
public async updateSaveasDraftEdit(MainList,userTitle,previousHistory,savedInEpic,finalFinanceComments,itemid)
{
  let ChildID;
  var itemHistory=previousHistory + "<br/>" +new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() + " - " + userTitle + " - " + " Request, Saved as Draft <br />";

  await
    pnp.sp.web.lists.getById(MainList).items.getById(itemid).update({
      //Title: firstName,
      TriggerFlow: false,        
      NotesFromFinanceTeam: finalFinanceComments==null?"":finalFinanceComments,
      CarrierInvoiceSavedInEpic: savedInEpic== true ? "Yes" : "No",
      ItemHistory:itemHistory,

    }).then((item: ItemAddResult) => {
      ChildID = item.data.ID;
    });
  return ChildID;
}
  public async getAccountType(listName) {
    var surveyConfigCollCat: any[] = [];
    try {
      const surveyConfigCollItems: any[] = await pnp.sp.web.lists.getByTitle(listName).items.select("ID,Title,Category").get();
      surveyConfigCollItems.forEach(item => {
        surveyConfigColl.push({
          key: item.ID.toString(), text: item.Title
        });
        surveyConfigCollCat.push({
          key: item.ID.toString(), text: item.Title,
          category:item.Category
        });
      });
    }
    catch (error) {
      alert(error);
    }
    return [surveyConfigColl,surveyConfigCollCat]
  }
  //get Status Master Data from Status list
  public async getStatusItems() {
    try {
      const dealerItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.Status).items.select("ID,Title")
      .filter("IsVisible eq 'Yes'") 
      .get();
      dealerItems.forEach(item => {
        statusOps.push({
          key: item.ID.toString(), text: item.Title
        });
      });
    }
    catch (error) {
      alert(error);
    }
    return statusOps;
  }
  //get Status Master Data from Status list
  public async getIssueTypeItems() {
    try {
      var issueTypeOps1=[];
      const dealerItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.IssueType).items.select("ID,Title").get();
      dealerItems.forEach(item => {
        issueTypeOps1.push({
          key: item.ID.toString(), text: item.Title
        });
      });
    }
    catch (error) {
      alert(error);
    }
    return issueTypeOps1;
  }
  
  public async getEOSurveyConfig(listName) {
    try {
      const surveyConfigCollItems: any[] = await pnp.sp.web.lists.getByTitle(listName).items.select("ID,Questions,Description,SurveyHomeText,SurveyHomeLink").get();
      surveyConfigCollItems.forEach(item => {
        surveyConfigColl.push({
          key: item.ID.toString(),
          Questions: item.Questions,
          Description: item.Description,
          SurveyHomeText: item.SurveyHomeText,
          SurveyHomeLink: item.SurveyHomeLink,
        });
      });
    }
    catch (error) {
      alert(error);
    }
    return surveyConfigColl;
  }
  public async getEmployeeList(emailId) {

    await pnp.sp.web.lists.getByTitle(ListName.EOSurveyEmployeeList).items.select("ID")
      .top(2000)
      .filter("Email eq '" + emailId + "'").getAll().then((vData: any[]) => {
        if (vData.length > 0)
          this.updateEmployeeList(vData[0].ID);
      });
  }
  public async updateEmployeeList(employeeID) {
    await pnp.sp.web.lists.getByTitle(ListName.EOSurveyEmployeeList).items.getById(employeeID).update({
      IsRespond: "Yes"
    });
  }
  public createFolder(siteURl: string, listname: string, folderName: string): Promise<any> {
    return pnp.sp.web.folders.getByName(listname).folders.add(folderName);
  }
  public uploadFile(siteURl: string, path: string, file: any, listItemID) {
    let fileNameParams: string[] = file.name.split('.');
    let fileName: string = fileNameParams[0] + "." + fileNameParams[1];
    //let web: any =  Web(siteURl);
    pnp.sp.web.getFolderByServerRelativeUrl(path).files.add(file.name, file, true)
      .then(({ file }) => file.getItem()).then((item: any) => {
        //  .then((r:any) => {
        item.update({
          ItemID: listItemID
        }).then((myupdate) => {
          console.log(myupdate);
          console.log("Metadata Updated");
        });
        return item;
      });
  }
  public async getAddCompanyFile(ItemId, currentContext, listName) {
    let fileInfo: any[] = [];
    let obj = {};
    const item: any = pnp.sp.web.lists.getByTitle(listName).items.getById(ItemId);
    await pnp.sp.web.lists.getByTitle(listName).items.getById(ItemId)
      .select("Id,Title,Attachments,AttachmentFiles")
      .expand("AttachmentFiles")
      .get().then((vData: any[]) => {
        for (var i = 0; i < vData["AttachmentFiles"].length; i++) {
          var _ServerRelativeUrl = vData["AttachmentFiles"][i].ServerRelativeUrl;
          let url: string = currentContext + "/_layouts/download.aspx?SourceUrl=" +
            encodeURIComponent(_ServerRelativeUrl);
          obj["Url"] = url;
          obj["fileName"] = vData["AttachmentFiles"][i].FileName;
          fileInfo.push(obj);
        }
      });
    return fileInfo;

  }
  public async updateStatusToAddCompanyList(statusData,updatedDescripton,currentUserTitle, previousComments, approveRejectComments, MainList, itemid) {
    let ChildID;
    try{   
    var vrapproveRejectComments = approveRejectComments == undefined ? "" : approveRejectComments;
    var previousCommentstxt = previousComments == null || previousComments==undefined  ? "" : previousComments;
    var mgrComments = previousCommentstxt + "<br/>" + new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString()+" - "+currentUserTitle + "<br/>" + statusData + "<br/>" + vrapproveRejectComments;

    await
      pnp.sp.web.lists.getByTitle(MainList).items.getById(itemid).update({
        approverComments: mgrComments,
        Description:updatedDescripton,
        status: statusData
      }).then((item: ItemAddResult) => {
        ChildID = statusData;
      });
    }
    catch (error) {
      alert("updateStatusToAddCompanyList " + error)
    }
    return ChildID;
  }
  public async updateEpicDetailsList(companyData, epicCompanyName, MainList, itemid) {
    let ChildID;
    await pnp.sp.web.lists.getByTitle(MainList).items.getById(itemid).update({
      UniqCompany: companyData[0].UniqCompany != undefined ? companyData[0].UniqCompany.toString() : "",

      LookupCode: companyData[0].LookupCode != undefined ? companyData[0].LookupCode.toString() : "",
      Company: companyData[0].Company != undefined ? companyData[0].Company.toString() : "",
      cType: companyData[0].Type != undefined ? companyData[0].Type.toString() : "",
      EOGroupInsCo: companyData[0].EOGroupInsCo != undefined ? companyData[0].EOGroupInsCo.toString() : "",
      Domicile: companyData[0].Domicile != undefined ? companyData[0].Domicile.toString() : "",
      EpicCompanyName: epicCompanyName,
      Status: "Final Approve"
    }).then((item: ItemAddResult) => {
      ChildID = item.data.ID;
    });
    return ChildID;
  }
  public async updateDataToAddCompnayList(udpateData,currentUserTitle,method, contactType, firstName, lastName,
    nameOnAccount, directType, accountType, countryOfOperation,
    accountNumber, routingNumber, institutionNumber,
    transitNumber, serviceClassCode, nameofInstitution,
    instittionPhoneNumber, authorizationStatus, authorizationDate,
    previousComments, approveComments,descriptionCommentsText,commentsrchText, MainList, itemid) {
    let ChildID;
    var vrapproveRejectComments = approveComments == undefined || approveComments == null ? "" : approveComments;
    var finalApproveComments = previousComments + "<br/>" + new Date().toLocaleDateString() + " " + new Date().toLocaleTimeString() +" - "+currentUserTitle +  "<br/>" + vrapproveRejectComments;
    await
      pnp.sp.web.lists.getByTitle(MainList).items.getById(itemid).update({
        companyname: udpateData[0].CompanyName,
        LookupCode: udpateData[0].LookUpCode,
        Title: firstName,
        LastName: lastName,
        Method: method,
        NameOnAccount: nameOnAccount,
        AccountNumber: accountNumber,
        RoutingNumber: routingNumber,
        InstitutionNumber: institutionNumber,
        TransitNumber: transitNumber,
        NameOfInstitution: nameofInstitution,
        InstitutionPhoneNumber: instittionPhoneNumber,
        AuthorizationDate: new Date(authorizationDate),
        approverComments: finalApproveComments,
        ContactType: contactType,
        AccountTypeId: accountType,
        DirectType: directType,
        CountryOfOperation: countryOfOperation,
        ServiceClassCode: serviceClassCode,
        AuthorizationStatus: authorizationStatus,
        Comments:commentsrchText,
        Description:descriptionCommentsText,
        
        // IfInsuranceCompanyisthisAdmitted: IfInsuranceCompanyisthisAdmitted,
        // NonAdmittedSurplusLinesComments: NotAdmittedomments,
        // IfthisisaNonAdmittedSurplusLines: IfthisisaNonAdmittedSurplusLines,
        // IsthisaForeignInsuranceCompanyor: IsthisaForeignInsuranceCompanyorState,

        status: "Submitted",
        //  Document_Unique_ID: guid
        // attachments
      }).then((item: ItemAddResult) => {
        ChildID = item.data.ID;

      });
    return ChildID;

  }
  
  //This will get ManagerName only by sending current user name
  private async _getManagerName(mgrUserName: string) {
    var properties = {};
    await pnp.sp.profiles.getPropertiesFor(mgrUserName).then((profile: any) => {
      profile.UserProfileProperties.forEach(function (prop) {
        properties[prop.Key] = prop.Value;
      });
      profile.userProperties = properties;
    });
    if (properties["Manager"] != "")
      await this._getManagerDetails(properties["Manager"]);
  }
  //To get more details about manager by sending manager name
  private async _getManagerDetails(user: string) {
    await pnp.sp.profiles.getPropertiesFor(user).then((profile: any) => {
      var properties = {};
      profile.UserProfileProperties.forEach(function (prop) {
        properties[prop.Key] = prop.Value;
      });
      profile.userProperties = properties;
      managerName = properties['FirstName'] + " " + properties['LastName'];
    });
  }
  public async getCurrentUserDetail() {
    UserDetails = [];
    let currentUserDetail = (await pnp.sp.web.currentUser.get());
    //await this._getManagerName(currentUserDetail.LoginName)
    UserDetails.push({
      ID: currentUserDetail.Id,
      Email: currentUserDetail.Email,
      Title: currentUserDetail.Title,
      Manager: managerName,
    });
    return UserDetails;
  }
  public async getCompanyDetails(id, listName) {
    let result = [];
    await pnp.sp.web.lists.getById(listName).items
      .select("*,Author/Title,Author/ID")
      .expand("Author")
      .filter("ID eq '" + id + "'")
      .get().then((vData: any[]) => {
        result = vData;
      });
    return result;
  }
  public async getAPHandlerDetails(listName) {
    let result = [];
    await pnp.sp.web.lists.getByTitle(listName).items
      .select("ID,Count,APHandler/ID,APHandler/Title")
      .expand("APHandler")
      .getAll().then((vData: any[]) => {
        vData.sort(function (a, b) {
          var x = a.Count < b.Count ? -1 : 1;
          return x;
        });
        result = vData[0];
      });
    return result;
  }

  public async AddCompanyToPayableList(companyData, updateCompanyData) {
    let ChildID;
    await
      pnp.sp.web.lists.getByTitle(ListNames.CompanyPayableList).items.add({
        APHandlerId: companyData.APHandler.ID,
        //PreviousAPHandlerId:companyData.APHandler.ID,
        LookupCode: updateCompanyData[0].LookupCode,
        Title: updateCompanyData[0].Company != undefined ? updateCompanyData[0].Company.toString() : "",
      });
    return ChildID;
  }
  public async getUserTitle() {
    let currentUserTitle = (await pnp.sp.web.currentUser.get()).Title;
    return currentUserTitle.toString();
  }
  public async getUserId(siteUrl: string) {
    let schedUseID = (await pnp.sp.web.currentUser.get()).Id;
    return schedUseID;
  }
  public updateStatus(id)
  {
    var promise = new Promise(async (resolve, reject) => {
     // alert("inside")
      resolve(id);
    });
    return promise;
  }
  public async checkIfUserExistInApproverList(id) {
    let result: boolean;
    await pnp.sp.web.lists.getByTitle(ListName.BillingApprovers).items.select("ID")
      .filter("ApproversId eq '" + id + "'").getAll().then((vData: any[]) => {
        if (vData.length > 0) {
          result = false;
        }
        else {
          result = true;
        }
      });
    return result;
  }
  public async checkIfUserSubmittedSurvey(id) {
    let result: boolean;
    await pnp.sp.web.lists.getByTitle(ListName.EOSurveyMain).items.select("ID")
      .filter("AuthorId eq '" + id + "'").getAll().then((vData: any[]) => {
        if (vData.length > 0) {
          result = true;
        }
        else {
          result = false;
        }
      });
    return result;
  }
  public async LookupCode() {
    try {
      const LookupCodeItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.LookupCode).items.select("ID,Title").get();
      LookupCodeItems.forEach(item => {
        LookupCode.push({
          key: item.ID.toString(), text: item.Title
        });
      });
    }
    catch (error) {
      console.log(error)
    }
    return LookupCode;
  }
  public async getConfigListData(listName) {
    var surveyConfigColl = [];
    try {
      await pnp.sp.web.lists.getByTitle(listName).items.select("ID,Title,HeaderDetail")
        .get().then((vData: any[]) => {
          if (vData.length > 0) {
            surveyConfigColl.push({
              ID: vData[0].ID.toString(),
              Title: vData[0].Title,
              HeaderDetail: vData[0].HeaderDetail,
            });
          }
        });
    }
    catch (error) {
      alert(error);
    }
    return surveyConfigColl;
  }
  public async cType() {
    try {
      const LookupCodeItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.Type).items.select("ID,Title").get();
      LookupCodeItems.forEach(item => {
        type.push({
          key: item.ID.toString(), text: item.Title
        });
      });
    }
    catch (error) {
      console.log(error)
    }
    return type;
  }
  public async Domicile() {
    try {
      const LookupCodeItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.Domicile).items.select("ID,Title").get();
      LookupCodeItems.forEach(item => {
        Domicile.push({
          key: item.ID.toString(), text: item.Title
        });
      });
    }
    catch (error) {
      console.log(error)
    }
    return Domicile;
  }
  public async EOGroupInsCo() {
    try {
      const LookupCodeItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.EOGroupInsCo).items.select("ID,Title").get();
      LookupCodeItems.forEach(item => {
        EOGroupInsCo.push({
          key: item.ID.toString(), text: item.Title
        });
      });
    }
    catch (error) {
      console.log(error)
    }
    return EOGroupInsCo;
  }
  // public async getEmployeeDataList() {

  //   await pnp.sp.web.lists.getByTitle(ListName.EOSurveyEmployeeList).items.select("IsRespond")
  //     .getAll().then((vData: any[]) => {
  //       totalEmployee = vData.length;
  //       isRespond = 0;
  //       vData.forEach(element => {
  //         if (element.IsRespond == "Yes")
  //           isRespond = isRespond + 1;
  //       });
  //       totalNotrespond = totalEmployee - isRespond;
  //     });
  //   return [totalEmployee, isRespond, totalNotrespond];
  // }
  // public async updateDataToWorkToolQuoteLst(ToUser, CC, Subject, QuoteReqNo, PhoneNo, FaxNo,
  //   MobileNumber, SalesComments, schedPurchComments, location, dealerQuoteNo, agent, salesMan, customer,
  //   designedLike, tRDTime, application, clarification, similarItem, drawings, estWeight, specialDiscount,
  //   quotedToDealerDate, availabilityReqDate, type, dealerIdSelectedKey, segmentIdSelectedKey, firstBuildIdSelectedKey
  //   , noQuoteNoQuoteStatus, currencyCode, dealerCode, jdeno, currentUserDetail, noQuoteComments, enginComments,
  //   EngDateTimeStarted1, EngDateTimeStarted2, EngDateTimeStarted3, PriceDateTimeStarted1, PriceDateTimeStarted2, PriceDateTimeStarted3,
  //   engDateTimeComp1, engDateTimeComp2, engDateTimeComp3, priceDateTimeComp1, priceDateTimeComp2, priceDateTimeComp3,
  //   engCompletedBy, engCompletedBy2, engCompletedBy3, priceCompletedBy1, priceCompletedBy2, priceCompletedBy3,
  //   noQuote, availability, completedSales, completedScheduling, completedPurchasing,
  //   completedSalesBy, completedSchedulingBy, completedPurchasingBy, completedSalesOn, completedSchedulingOn, completedPurchasingOn,
  //   engComp, engComp2, engComp3, priceComp1, priceComp2, priceComp3, pricing, quotedToDealer, quotedToDealerBy
  // ) {
  //   let ItemId = 144;
  //   let ChildID;
  //   await
  //     pnp.sp.web.lists.getByTitle(ListName.WorkToolsQuote).items.getById(ItemId).update({

  //       ToId: { results: ToUser },
  //       CCId: { results: CC },
  //       Subject: Subject,
  //       //TakerId: currentUserDetail,
  //       //SchedUse:currentUserDetail,
  //       QuoteRequestNo: QuoteReqNo,
  //       //Date: new Date(),
  //       SalesComments: SalesComments,
  //       Location: location,
  //       DealerQuoteNo: dealerQuoteNo,
  //       DealerCode: dealerCode,
  //       JDENumber: jdeno,
  //       PhoneNumber: PhoneNo,
  //       MobilePhoneNo: MobileNumber,
  //       FaxNo: FaxNo,
  //       SchedPurchComments: schedPurchComments,
  //       Agent: agent,
  //       SalesMan: salesMan,
  //       Customer: customer,
  //       Availability_ReqDate: availabilityReqDate,
  //       WTPartsQuoteType: type,
  //       DealerId: dealerIdSelectedKey != "" ? dealerIdSelectedKey : null,
  //       SegmentId: segmentIdSelectedKey != "" ? segmentIdSelectedKey : null,
  //       FirstBuildId: firstBuildIdSelectedKey != "" ? firstBuildIdSelectedKey : null,
  //       CurrencyCode: currencyCode,
  //       NoQuote_Comments: noQuote == true ? noQuoteComments : "",
  //       SpecialDiscount: specialDiscount,
  //       EngineeringComments: enginComments,
  //       DesignedLike: designedLike,
  //       TRDTime: tRDTime,
  //       Application: application,
  //       Clarification: clarification,
  //       SimilarItem: similarItem,
  //       Drawings: drawings,
  //       EstWeight: estWeight,
  //       NoQuote: noQuote == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       Availability: availability == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       Pricing: pricing == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       QuotedToDealer: quotedToDealer == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       Completed_Sales: completedSales == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       Completed_Scheduling: completedScheduling == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       Completed_Purchasing: completedPurchasing == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       EngComp_1: engComp == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       EngComp_2: engComp2 == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       EngComp_3: engComp3 == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       PriceComp_1: priceComp1 == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       PriceComp_2: priceComp2 == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       PriceComp_3: priceComp3 == true ? { results: ["Yes"] } : { results: ["No Type"] },
  //       QuotedToDealer_By: quotedToDealerBy,
  //       QuotedToDealer_Date: quotedToDealerDate,
  //       Completed_Sales_By: completedSales == true ? completedSalesBy : null,
  //       Completed_Scheduling_By: completedScheduling == true ? completedSchedulingBy : null,
  //       Completed_Purchasing_By: completedPurchasing == true ? completedPurchasingBy : null,
  //       Completed_Sales_On: completedSales == true ? completedSalesOn : null,
  //       Completed_Scheduling_On: completedScheduling == true ? completedSchedulingOn : null,
  //       Completed_Purchasing_On: completedPurchasing == true ? completedPurchasingOn : null,
  //       Eng_DateTimeStarted_1: engComp == true ? EngDateTimeStarted1 : null,

  //       Eng_DateTimeStarted_2: engComp2 == true ? EngDateTimeStarted2 : null,
  //       Eng_DateTimeStarted_3: engComp3 == true ? EngDateTimeStarted3 : null,
  //       Price_DateTimeStarted_1: priceComp1 == true ? PriceDateTimeStarted1 : null,
  //       Price_DateTimeStarted_2: priceComp2 == true ? PriceDateTimeStarted2 : null,
  //       Price_DateTimeStarted_3: priceComp3 == true ? PriceDateTimeStarted3 : null,

  //       Eng_DateTimeComp_1: engComp == true ? engDateTimeComp1 : null,
  //       Eng_DateTimeComp_2: engComp2 == true ? engDateTimeComp2 : null,
  //       Eng_DateTimeComp_3: engComp3 == true ? engDateTimeComp3 : null,
  //       Price_DateTimeComp_1: priceComp1 == true ? priceDateTimeComp1 : null,
  //       Price_DateTimeComp_2: priceComp2 == true ? priceDateTimeComp2 : null,
  //       Price_DateTimeComp_3: priceComp3 == true ? priceDateTimeComp3 : null,


  //       Eng_CompletedBy_1: engComp == true ? engCompletedBy : null,
  //       Eng_CompletedBy_2: engComp2 == true ? engCompletedBy2 : null,
  //       Eng_CompletedBy_3: engComp3 == true ? engCompletedBy3 : null,
  //       Price_CompletedBy_1: priceComp1 == true ? priceCompletedBy1 : null,
  //       Price_CompletedBy_2: priceComp2 == true ? priceCompletedBy2 : null,
  //       Price_CompletedBy_3: priceComp3 == true ? priceCompletedBy3 : null,

  //       NoQuote_NoQuote: noQuote == true ? noQuoteNoQuoteStatus : null,


  //     }).then((item: ItemAddResult) => {
  //       ChildID = item.data.ID;

  //     });
  //   return ChildID;
  // }

  // public async getDealerItems() {
  //   const dealerItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.DealerList).items.select("ID,Title,DealerCode,JDENo,Location").get();
  //   dealerTitles1 = [];
  //   dealerItems.forEach(item => {
  //     dealerTitles1.push({
  //       key: item.ID.toString(), text: item.Title
  //     });
  //     dealerTitlesColl.push({
  //       key: item.ID.toString(),
  //       DealerCode: item.DealerCode,
  //       JDENo: item.JDENo,
  //       DealerLocation: item.Location,

  //     });
  //   });
  //   return [dealerTitles1, dealerTitlesColl];
  //   //  return dealerTitles1;
  // }
  // public async getSegmentItems() {
  //   const dealerItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.SegmentList).items.select("ID,Title").get();
  //   dealerItems.forEach(item => {
  //     segment.push({
  //       key: item.ID.toString(), text: item.Title
  //     });
  //   });
  //   return segment;
  // }
  // public async getUserId() {
  //   let schedUseID = (await pnp.sp.web.currentUser.get()).Id;
  //   return schedUseID;
  // }
  // public async getUserTitle() {
  //   let schedUseTitle = (await pnp.sp.web.currentUser.get()).Title;
  //   return schedUseTitle.toString();
  // }
  // public async saveWorkToolQuoteQuantityData(masterID, ReactTableResult, quoteRequestNo) {
  //   //debugger;
  //   for (var iID = 0; iID < ReactTableResult.length; iID++) {
  //     try {
  //       pnp.sp.web.lists.getByTitle(ListName.QtySpecificationMasterList).items.add({
  //         Quantity: ReactTableResult[iID]["Quantity"],
  //         Title: ReactTableResult[iID]["ItemNumber"].toString(),
  //         Machine: ReactTableResult[iID]["Machine"].toString(),
  //         Description: ReactTableResult[iID]["Description"].toString(),
  //         ListPrice: ReactTableResult[iID]["ListPrice"],
  //         Avail_WKS: ReactTableResult[iID]["Avail_WKS"].toString(),
  //         QuoteType: ReactTableResult[iID]["QuoteType"].toString(),
  //         SerialNumber: ReactTableResult[iID]["SerialNumber"].toString(),
  //         WorkTool: ReactTableResult[iID]["WorkTool"].toString(),
  //         DealerNet: ReactTableResult[iID]["DealerNet"],
  //         RefNo: ReactTableResult[iID]["RefNo"].toString(),
  //         QuoteRequestIDId: masterID,
  //         QuoteRequestNumberId: quoteRequestNo
  //       });
  //     } catch (error) {
  //       console.log("Error in addItems : " + error);
  //     }
  //   }
  // }

  // public async updateWorkToolQuoteQuantityData(masterID, ReactTableResult, quoteRequestNo) {  //debugger;
  //   for (var iID = 0; iID < ReactTableResult.length; iID++) {
  //     try {
  //       pnp.sp.web.lists.getByTitle(ListName.QtySpecificationMasterList).items.getById(1).update({
  //         Quantity: ReactTableResult[iID]["Quantity"],
  //         Title: ReactTableResult[iID]["ItemNumber"].toString(),
  //         Machine: ReactTableResult[iID]["Machine"].toString(),
  //         Description: ReactTableResult[iID]["Description"].toString(),
  //         ListPrice: ReactTableResult[iID]["ListPrice"],
  //         Avail_WKS: ReactTableResult[iID]["Avail_WKS"].toString(),
  //         QuoteType: ReactTableResult[iID]["QuoteType"].toString(),
  //         SerialNumber: ReactTableResult[iID]["SerialNumber"].toString(),
  //         WorkTool: ReactTableResult[iID]["WorkTool"].toString(),
  //         DealerNet: ReactTableResult[iID]["DealerNet"],
  //         RefNo: ReactTableResult[iID]["RefNo"].toString(),
  //         QuoteRequestIDId: masterID,
  //         QuoteRequestNumberId: quoteRequestNo
  //       });
  //     } catch (error) {
  //       console.log("Error in addItems : " + error);
  //     }
  //   }
  // }

  // public async getWorkToolQuoteQuantityData() {

  // }
  // public async getQuoteMasterRequestNumber() {
  //   const QuoteMasterItem: any[] = await pnp.sp.web.lists.getByTitle(ListName.QuoteRequestMasterList).items.select("Title").get();
  //   QuoteMasterItem.forEach(item => {
  //     quoteMasterRecord = item.Title
  //   });
  //   return quoteMasterRecord;
  // }
  // public async getFirstBuildItems() {
  //   const dealerItems: any[] = await pnp.sp.web.lists.getByTitle(ListName.FirstBuildList).items.select("ID,Title").get();
  //   firstBuild = [];
  //   dealerItems.forEach(item => {
  //     firstBuild.push({
  //       key: item.ID.toString(), text: item.Title
  //     });
  //   });
  //   return firstBuild
  // }

  // //Get Work Tool Sales order List Data
  // public async getWorkToolWTPartsQuote() {
  //   debugger;
  //   let salesviewItem;
  //   await pnp.sp.web.lists
  //     .getByTitle(ListName.WorkToolsQuote)
  //     .items.select('JDEOrderNo', 'OrderType', 'JDEEnteredDate', 'Subject', 'ID')
  //     .get()
  //     .then((item: any[]) => {
  //       salesviewItem = item
  //     });
  //   return salesviewItem;

  // }

  // public async getWorkToolWTPartsQuoteById(ItemId: number) {
  //   debugger;
  //   let salesItem;
  //   await pnp.sp.web.lists
  //     .getByTitle(ListName.WorkToolsQuote)
  //     .items.select('JDEOrderNo', 'OrderType', 'JDEEnteredDate', 'Subject', 'ID')
  //     .getById(ItemId)
  //     .get()
  //     .then((item: any[]) => {
  //       salesItem = item
  //     });
  //   return salesItem;

  // }
  // public async updateQuoteRequestMasterList(NewQuoteNumber) {
  //   const i = await pnp.sp.web.lists.getByTitle(ListName.QuoteRequestMasterList).items.getById(1).update({
  //     Title: NewQuoteNumber,
  //   });
  //   console.log(i);
  // }
  // public async getGropsNames() {
  //   let tblStructure = "<table>"
  //   let groups = "";
  //   await pnp.sp.web.siteGroups.get().then((items: any[]) => {
  //     items.map((item, i) => {
  //       groups += "<tr><td>" + items[i].Title + '<td/><tr/>';
  //     });
  //   });
  //   tblStructure += groups + "</table>";
  //   return tblStructure;
  // }
  // //get specification list data
  // public async getSpecificationChildData() {
  //   let ItemId = 128, itemCollecton = [];
  //   await pnp.sp.web.lists.getByTitle(ListName.QtySpecificationMasterList).items.select("ID", "Title", "Description",
  //     "Quantity", "Machine", "SerialNumber", "WorkTool", "DealerNet", "Avail_WKS", "QuoteType", "ListPrice",
  //     "QuoteRequestID/Id", "QuoteRequestID/Title", "RefNo"
  //   )
  //     .expand("QuoteRequestID")
  //     .filter("QuoteRequestIDId eq '" + ItemId + "'").getAll().then((vData: any[]) => {
  //       itemCollecton = vData;
  //     });
  //   return itemCollecton;
  // }

  // public async getNoQuoteMultiData() {

  // }
  // public async deleteQtyRepeatingData(DeleteListItems) {
  //   debugger;
  //   let list = await pnp.sp.web.lists.getByTitle(ListName.QtySpecificationMasterList);
  //   const items = DeleteListItems.map((item, i) => {
  //     if (item.id != 0) {
  //       list.items.getById(item).delete();
  //     }
  //     // else{
  //     //   this.SaveSalesOrederRepeatingData(item);
  //     // } 

  //   });
  // }

  // public async updateQtyRepeatingData(updateItmesRepeat) {
  //   debugger;
  //   let list = await pnp.sp.web.lists.getByTitle(ListName.QtySpecificationMasterList);
  //   const items = updateItmesRepeat.map((item, i) => {
  //     if (item.ID != undefined) {
  //       list.items.getById(item.ID).update({
  //         Quantity: item.Quantity,
  //         Title: item.ItemNumber,
  //         Description: item.Description,
  //         DealerNet: item.DealerNet,
  //         ListPrice: item.ListPrice,
  //         Machine: item.Machine,
  //         Avail_WKS: item.PurchManul,
  //         QuoteType: item.Facility,
  //         WorkTool: item.WorkTool,
  //         RefNo: item.RefNo,
  //       });
  //     }
  //     // else{
  //     //   list.items.add({
  //     //   Quantity: item.Quantity,             
  //     //   SalesOrederId:item.SalesOrederId
  //     //   });
  //     // }
  //   });

  // }

  // public async AddItemInSalesHistoryList(toIds, From, currentDate, workToolId, qtReqNo) {

  //   await pnp.sp.web.lists.getByTitle(ListName.WorkToolHistoryList).items.add({
  //     SentToId: { results: toIds },
  //     SentFromId: From,
  //     DateSent: currentDate,
  //     WorkToolId: workToolId,
  //     QuoteRequestNumber: qtReqNo,

  //   }).then((result: ItemAddResult) => {
  //   }).catch(error => {
  //     console.log(error);
  //     alert('Error in item Creation in funtion AddItemInSalesHistoryList. check console.');
  //   });
  // }
  // public async UpdateSendMailDetailsInSalesOrder(itemId, mailSentUser, mailSentDateTime) {

  //   await pnp.sp.web.lists.getByTitle(ListName.WorkToolsQuote).items.getById(itemId).update({
  //     MailSentUser: mailSentUser,
  //     MailSentDateTime: mailSentDateTime

  //   }).then((result: ItemAddResult) => {
  //     console.log(result);
  //     console.log('Item Updated Successfully');
  //   }).catch(error => {
  //     console.log(error);
  //     alert('Error in item Updation in function UpdateSendMailDetailsInSalesOrder. check console.');
  //   });
  // }
  // public ConvertDateToDateTime(date: Date) {
  //   var hours = date.getHours();
  //   var ampm = hours >= 12 ? 'pm' : 'am';
  //   hours = hours % 12;
  //   hours = hours ? hours : 12; // the hour '0' should be '12'

  //   let dformat = [('0' + (date.getMonth() + 1)).slice(-2),
  //   ('0' + date.getDate()).slice(-2),
  //   date.getFullYear()].join('/') + ' ' +
  //     [('0' + hours).slice(-2),
  //     ('0' + date.getMinutes()).slice(-2),
  //     ('0' + date.getSeconds()).slice(-2),
  //     ].join(':');
  //   return (dformat + ' ' + ampm);
  // }
  //  
}