
// List Name configuration
export default class ListNames {
  public static EOSurveyMain: string = "EOSurveyMain";
  public static EOSurveyEmployeeList: string = "EOSurveyEmployeeList";
  public static ConfigList: string = "ConfigList";
  public static LookupCode: string = "LookupCode";
  public static Type: string = "Type";
  public static Domicile: string = "Domicile";
  public static EOGroupInsCo: string = "EOGroupInsCo";
  public static APHandler: string = "APHandler";
  public static CompanyPayableList: string = "CompanyPayableList";

  public static Status: string = "Status";
  public static IssueType: string = "IssueType";
public static BillingApprovers: string = "BillingApprovers";
}