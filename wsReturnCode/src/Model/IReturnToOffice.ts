export interface IReturnToOfficeCreateState {
  items: any[];
  isAlreadyOpted: boolean;
  firstName: string;
  Ques1: Date;
  Ques2: string;
  Ques3: string;
  Ques4: boolean;
  Ques5: string;
  Ques6: string;
  Ques7: boolean;

  Ques1Status: boolean,
  Ques2Status: boolean,
  Ques3Status: boolean,
  Ques4Status: boolean,
  Ques5Status: boolean,
  Ques6Status: boolean,
  Ques7Status: boolean,

  isHideQues1Status: boolean,
  isHideQues2Status: boolean,
  isHideQues3Status: boolean,
  isHideQues4Status: boolean,
  isHideQues5Status: boolean,
  isHideQues6Status: boolean,
  isHideQues7Status: boolean,

  ddlQues1Status: string,
  ddlQues2Status: string,
  ddlQues3Status: string,
  ddlQues4Status: string,
  ddlQues5Status: string,
  ddlQues6Status: string,
  ddlQues7Status: string,
  additionalComments: string,

  AttestedQues1: boolean;
  AttestedQues2: boolean;
  AttestedQues3: boolean;
  AttestedQues4: boolean;
  AttestedQues5: boolean;
  AttestedQues6: boolean;
  AttestedQues7: boolean;
  AttestedQues8: boolean;
  AttestedQues9: boolean;
  iAgree: boolean;
  isLoadingImage: boolean;
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,
  selectedReqDate: Date,
  isARMember: boolean,
  isOfficeAdmin: boolean,
  Comments: string,
  isApproved: boolean,
  badgeID: string,
  isCompleted: boolean,
  isSubmitted: boolean,
  Signature: string,
  SubmitDate: Date,
  previousDocument: any;
  isCreatedBy: boolean;
  IsEditEnabled:boolean;
  isdataLoaded:boolean,
  Office:string

}
export interface IReturnToOfficeEditState {
  items: any[];
  isAlreadyOpted: boolean;
  firstName: string;
  Billingdate: Date;
  Scheduleddate: Date;
  Applicationdate: Date;
  NotesServiceTeam: string;
  NotesFinanceTeam: string;
  Client: string;
  Carrier: string;
  EpicInvoiceNumber: string;
  Amount: string;
  IssueTypeChangeSelectedKey: string;
  CurrentLevel: string;
  savedInEpic: boolean;
  AssignedtoEpic: string;
  isDataReceived: boolean;
  nowLoadRichText: boolean;
  uploadDocInfo: any;
  previousDocument: any;
  AccountHandler: any;
  toAccountHandlerEmails, toEditAccountHandlerEmails: any;
  isProfileViewMode: boolean;
  CurrentStatus: string;
  isClosed, isAPTeam: boolean;
  isLoadingImage: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
  policyNumber: "",
  FilteredListItems: any,
  showGrid: boolean,
  StatusChangeTitleSelectedKey: string;
  financeCommentsSummary: string,
  SalesCommentsSummary: string,
  StatusChangeSelectedTitle: string,
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,
  requestorSection: {},
  IsEditEnabled: boolean,
  isRequestExists: boolean,
  FormMode: "Approve",
  attachmentSection: [],
  acceptedFiles: [],
  newAttachments: [],
  loading: boolean,
  isAuthorized: boolean,
  isAuthorizedUser: boolean,
  isServiceCollapse: boolean,
  itemHistorySummary: string,
  Office:string

}
export interface IUnAppliedCashCreateState {
  items: any[];
  isAlreadyOpted: boolean;
  firstName: string;
  Billingdate: Date;
  Scheduleddate: Date;
  Applicationdate: Date;
  NotesServiceTeam: string;
  NotesFinanceTeam: string;
  Client: string;
  Carrier: string;
  EpicInvoiceNumber: string;
  Amount: string;
  IssueTypeChangeSelectedKey: string;
  CurrentLevel: string;
  savedInEpic: boolean;
  AssignedtoEpic: string;
  isDataReceived: boolean;
  nowLoadRichText: boolean;
  uploadDocInfo: any;
  previousDocument: any;
  AccountHandler: any;
  toAccountHandlerEmails, toEditAccountHandlerEmails: any;
  isProfileViewMode: boolean;
  CurrentStatus: string;
  isClosed, isAPTeam: boolean;
  isLoadingImage: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
  policyNumber: "",
  FilteredListItems: any,
  showGrid: boolean,
  StatusChangeTitleSelectedKey: string;
  financeCommentsSummary: string,
  SalesCommentsSummary: string,
  StatusChangeSelectedTitle: string,
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,

}
export interface IUnAppliedCashEditState {
  items: any[];
  isAlreadyOpted: boolean;
  firstName: string;
  Billingdate: Date;
  Scheduleddate: Date;
  Applicationdate: Date;
  NotesServiceTeam: string;
  NotesFinanceTeam: string;
  Client: string;
  Carrier: string;
  EpicInvoiceNumber: string;
  Amount: string;
  IssueTypeChangeSelectedKey: string;
  CurrentLevel: string;
  savedInEpic: boolean;
  AssignedtoEpic: string;
  isDataReceived: boolean;
  nowLoadRichText: boolean;
  uploadDocInfo: any;
  previousDocument: any;
  AccountHandler: any;
  toAccountHandlerEmails, toEditAccountHandlerEmails: any;
  isProfileViewMode: boolean;
  CurrentStatus: string;
  isClosed, isARTeam: boolean;
  isLoadingImage: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
  policyNumber: "",
  FilteredListItems: any,
  showGrid: boolean,
  StatusChangeTitleSelectedKey: string;
  financeCommentsSummary: string,
  SalesCommentsSummary: string,
  StatusChangeSelectedTitle: string,
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,
  requestorSection: {},
  IsEditEnabled: boolean,
  isRequestExists: boolean,
  FormMode: "Approve",
  attachmentSection: [],
  acceptedFiles: [],
  newAttachments: [],
  loading: boolean,
  isAuthorized: boolean,
  isAuthorizedUser: boolean,
  isServiceCollapse: boolean,
  itemHistorySummary: string,

}
export interface IRequestPaymentCreateState {
  items: any[];
  isAlreadyOpted: boolean;
  NotesFinanceTeam: string;
  Client: string;
  Carrier: string;
  EpicInvoiceNumber: string;
  Amount: string;
  CurrentLevel: string;
  savedInEpic: boolean;
  CarrierProvidedStatement: boolean;
  AssignedtoEpic: string;
  isDataReceived: boolean;
  nowLoadRichText: boolean;
  uploadDocInfo: any;
  previousDocument: any;
  AccountHandler: any;
  toAccountHandlerEmails, toEditAccountHandlerEmails: any;
  isProfileViewMode: boolean;
  CurrentStatus: string;
  isClosed, isAPTeam: boolean;
  isLoadingImage: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
  policyNumber: "",
  FilteredListItems: any,
  showGrid: boolean,
  StatusChangeTitleSelectedKey: string;
  financeCommentsSummary: string,
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,
  selAPUser: any[],
  selAPUserValue: Ioption,
  selAPUserValueId: string,

}
export interface IRequestPaymentEditState {
  items: any[];
  isAlreadyOpted: boolean;
  NotesFinanceTeam: string;
  Client: string;
  Carrier: string;
  EpicInvoiceNumber: string;
  Amount: string;
  CurrentLevel: string;
  savedInEpic: boolean;
  CarrierProvidedStatement: boolean;
  AssignedtoEpic: string;
  isDataReceived: boolean;
  nowLoadRichText: boolean;
  uploadDocInfo: any;
  previousDocument: any;
  AccountHandler: any;
  toAccountHandlerEmails, toEditAccountHandlerEmails: any;
  isProfileViewMode: boolean;
  CurrentStatus: string;
  isClosed, isARTeam: boolean;
  isLoadingImage: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
  policyNumber: "",
  FilteredListItems: any,
  showGrid: boolean,
  //StatusChangeTitleSelectedKey:string;
  financeCommentsSummary: string,
  //SalesCommentsSummary:string,
  StatusChangeSelectedTitle: string,
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,
  requestorSection: {},
  IsEditEnabled: boolean,
  isRequestExists: boolean,
  FormMode: "Approve",
  attachmentSection: [],
  acceptedFiles: [],
  newAttachments: [],
  loading: boolean,
  isAuthorized: boolean,
  isAuthorizedUser: boolean,
  isServiceCollapse: boolean,
  selAPUser: Ioption,
  selAPUserValue: Ioption,
  selAPUserValueId: string,
  itemHistorySummary: string,

}
export interface IOffReBillNewState {
  items: any[];
  isAlreadyOpted: boolean;
  NotesFinanceTeam: string;
  Client: string;
  CurrentLevel: string;
  isDataReceived: boolean;
  nowLoadRichText: boolean;
  uploadDocInfo: any;
  previousDocument: any;
  AccountHandler: any;
  toAccountHandlerEmails,
  toEditAccountHandlerEmails: any;
  isProfileViewMode: boolean;
  CurrentStatus: string;
  isClosed;
  isLoadingImage: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
  FilteredListItems: any,
  showGrid: boolean,
  financeCommentsSummary: string,
  SalesCommentsSummary: string,
  StatusChangeSelectedTitle: string,
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,
  selAPUser: any[],
  selAPUserValue: Ioption,
  selAPUserValueId: string,

}
export interface IOffReBillEditState {
  items: any[];
  isAlreadyOpted: boolean;
  NotesFinanceTeam: string;
  Client: string;
  //Carrier: string;
  //EpicInvoiceNumber: string;
  //Amount: string;
  CurrentLevel: string;
  //savedInEpic: boolean;
  // CarrierProvidedStatement: boolean;
  //AssignedtoEpic: string;
  isDataReceived: boolean;
  nowLoadRichText: boolean;
  uploadDocInfo: any;
  previousDocument: any;
  AccountHandler: any;
  toAccountHandlerEmails, toEditAccountHandlerEmails: any;
  isProfileViewMode: boolean;
  CurrentStatus: string;
  isClosed, isARTeam: boolean;
  isLoadingImage: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
  //policyNumber:"",
  FilteredListItems: any,
  showGrid: boolean,
  //StatusChangeTitleSelectedKey:string;
  financeCommentsSummary: string,
  SalesCommentsSummary: string,
  StatusChangeSelectedTitle: string,
  isRejected: boolean,
  isCollapse: boolean,
  isFinanceCollapse: boolean,
  selectedvalue: Ioption,
  optionClient: Ioption[],
  isDataSelected: boolean,
  requestorSection: {},
  IsEditEnabled: boolean,
  isRequestExists: boolean,
  FormMode: "Approve",
  attachmentSection: [],
  acceptedFiles: [],
  newAttachments: [],
  loading: boolean,
  isAuthorized: boolean,
  isAuthorizedUser: boolean,
  isServiceCollapse: boolean,
  selAPUser: Ioption,
  selAPUserValue: Ioption,
  selAPUserValueId: string,
  itemHistorySummary: string,

}
export interface Ioption {
  value: string;
  label: string;
}

// companyName:string;
// lookupCode:string;
// method
// nameOnAccount:string;
// Description
// Type
// countryOfOperation:any;
// accountNumber:string;
// routingNumber:string;
// institutionNumber:string;
// transitNumber:string;
// serviceClassCode:any;
// nameofInstitution:string;
// instittionPhoneNumber:string;
// authorizationStatus:any;
// authorizationDate:Date;
// // comments:string
// FilteredListItems: any;
// accountName: string;
// uploadDocInfo: any;
// previousDocs: any;
// contactType: string;
// accountType: string;
// isProfileViewMode: boolean;
// firstName: string;
// lastName: string;
// isBusiness: boolean;
// nowLoadRichText:boolean;
// description:string;
// nameOnAccount:string;
// Comments:string;
// currEpicStatus:boolean;
// isViewForm:boolean;
// ISStatus:boolean;
// approveStatus:boolean;
// rejectStatus:boolean;
// isEpicStatusFinalApprove:boolean;
// statusData: string;
// previousDocument: any,
// approveComments:string,
// directType:string,
// companyname:string,
// lookupCode:string,
// PaymentMethodCode:any;
// CountryOfOperationOptions:any;
// PaymentMethodCodeOptions:any,
// DirectWithdrawalOptions:any,
// ServiceClassCodeOptions:any,
// accountTypeText:string,
// countryOfOperationText: string
//     serviceClassCodeText: string
//     authorizationStatusText: string
//     DirectWithdrawalText: string
//     PaymentMethodCodeText: string
//}
export interface IBillingAllDataState {
  CompanyName: string;
  AccountName: string;
  lookupCode: string;
  nameOnAccount: string;
  status: string;
  showGrid: boolean;
  FilteredListItems: any;
  isLoaded: boolean;
  isApprover: boolean;
  isAccountHandler: boolean,
  isCreatedBy: boolean,
  isNotRegularUser: boolean,
}